<?php
ob_start();
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Affordable Resort.</title>
  <meta charset="utf-8">
  <meta name="description" content="Calamba Laguna Resorts">
  <meta name="keywords" content="Affordable,Cheap,Pansol,Calamba,Laguna,Resorts,Resort">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <link rel="stylesheet" type="text/css" href="styles/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="styles/style.css">
  <link rel="stylesheet" href="styles/w3css.css">
  <script src="styles/jquery.min.js"></script>
  <script src="styles/bootstrap.min.js"></script>

<style>
  <?php require_once("styles/resortstyle.css");?>

  h3{ color:black}
  .mySlides {display:none}
  .demo {cursor:pointer}
</style>

<script>
function success(){
    alert("Congrats! Successfully submitted.\nWe will contact you as soon as we notice your request. Thank you.");
}
</script>

</head>
<body>
<?php
  require_once("header.php");
  require_once("admin/connection.php");
?>
<?php
//send the name and the id.
if(!empty($_GET['id'])){  // check if data has been sent or clicked
  $value = $_GET['id'];


$queryInfo = "SELECT name, address, pax, bedroom, aircon, sleep, facilities, amenities, otherExpenses 
FROM information WHERE id = $value";

$queryPrice = "SELECT pDay12Day, pDay12Night, pDay24, pEnd12Day, pEnd12Night, pEnd24, oDay12Day, oDay12Night, oDay24, oEnd12Day, oEnd12Night, oEnd24 FROM price WHERE priceId = $value";

$queryPool = "SELECT adultQuantity, aFirstFeet, aFirstSlide, aFirstTemp,
aSecondFeet, aSecondSlide, aSecondTemp, 
aThirdFeet, aThirdSlide,  aThirdTemp,
kidQuantity, kFirstFeet, kFirstSlide, kFirstTemp,
kSecondFeet, kSecondSlide, kSecondTemp,
kThirdFeet, kThirdSlide, kThirdTemp FROM pool WHERE poolId = $value";

  $responseInfo  = @mysqli_query($dbc, $queryInfo);
  $responsePrice = @mysqli_query($dbc, $queryPrice);
  $responsePool  = @mysqli_query($dbc, $queryPool);

  if($responseInfo && $responsePrice && $responsePool){
    $dataInfo  = mysqli_fetch_array($responseInfo);
    $dataPrice = mysqli_fetch_array($responsePrice);
    $dataPool  = mysqli_fetch_array($responsePool);

    $name = $dataInfo['name'];
?>
  <div class="container">
    <div class="row">
    <div class="container">
        <div class="col-sm-4 resortD">
   
<?php
    echo "<div align='center' class='b'><h1>".$dataInfo['name']."</h1></div>";
    echo"<h5><i class='b'>Price range</i></h5>";
    echo"<i>Off Season</i><br>";
      echo"<ul><li>Week day's&nbsp;";
       echo"(Monday-Thursday)</li>";
        echo"12hrs daytime rate: ".$dataPrice['oDay12Day']."<br>";
        echo"12hrs overnight rate: ".$dataPrice['oDay12Night']."<br>";
        echo"24hrs rate: ".$dataPrice['oDay24']."<br>";
      echo"<li>Week end's (Friday-Sunday)</li>";
        echo"12hrs daytime rate: ".$dataPrice['oEnd12Day']."<br>";
        echo"12hrs overnight rate: ".$dataPrice['oEnd12Night']."<br>";
        echo"24hrs rate: ".$dataPrice['oEnd24']."</ul>";

    echo"<i>Peak Season</i><br>";
      echo"<ul><li>Week day's&nbsp;";
       echo"(Monday-Thursday)</li>";
        echo"12hrs daytime rate: ".$dataPrice['pDay12Day']."<br>";
        echo"12hrs overnight rate: ".$dataPrice['pDay12Night']."<br>";
        echo"24hrs rate: ".$dataPrice['pDay24']."<br>";
        echo"<li>Week end's (Friday-Sunday)</li>";
        echo"12hrs daytime rate: ".$dataPrice['pEnd12Day']."<br>";
        echo"12hrs overnight rate: ".$dataPrice['pEnd12Night']."<br>";
        echo"24hrs rate: ".$dataPrice['pEnd24']."</ul>"."<hr>";        


    echo"<h5><i class='b'>Resort details</i></h5>";
    echo"QUANTITY REFERENCE<br>";        
    echo"Pax can accommodate: ".$dataInfo['pax']."<br>";
    echo"Bedrooms: ".$dataInfo['bedroom']."<br>";
    echo"Aircon: ".$dataInfo['aircon']."<br>";
    echo"Sleeping capacity: ".$dataInfo['sleep']."<br><br>";

    echo"POOLS:<br>";

    if(!empty($dataPool['kidQuantity']))
      echo $dataPool['kidQuantity']. " Kiddie pool:<br>";        
    if(!empty($dataPool['kFirstFeet']))
    echo $dataPool['kFirstFeet']." feet, ".$dataPool['kFirstTemp'].", ".$dataPool['kFirstSlide']."<br>";
    if(!empty($dataPool['kSecondFeet']))
      echo $dataPool['kSecondFeet']." feet, ".$dataPool['kSecondTemp'].", ".$dataPool['kSecondSlide']."<br>";
    if(!empty($dataPool['kThirdFeet']))
      echo $dataPool['kThirdFeet']." feet, ".$dataPool['kThirdTemp'].", ".$dataPool['kThirdSlide']."<br>";

    echo"<br>";

    echo $dataPool['adultQuantity']. " Adult pool:<br>";        
    echo $dataPool['aFirstFeet']." feet, ".$dataPool['aFirstTemp'].", ".$dataPool['aFirstSlide']."<br>";
    if(!empty($dataPool['aSecondFeet']))
      echo $dataPool['aSecondFeet']." feet, ".$dataPool['aSecondTemp'].", ".$dataPool['aSecondSlide']."<br>";
    if(!empty($dataPool['aThirdFeet']))
      echo $dataPool['aThirdFeet']." feet, ".$dataPool['aThirdTemp'].", ".$dataPool['aThirdSlide']."<br>";

    echo"<br><br>";

    echo "FACILITIES:<br>";
    echo $dataInfo['facilities']."<br><br>";        

    echo "AMENITIES:<br>";
    echo $dataInfo['amenities']."<br><br>";

    if(!empty($dataInfo['otherExpenses'])){
      echo "Other Expenses:<br>";
      echo $dataInfo['otherExpenses']."<br><hr>";
    }
    ?>  <hr>

<?php
require_once("reservation.php");
?>

      
    <?php
    echo "<center><h4>Or</h4>";
    echo "<center><h3>Direct contact us!</h3>";
    echo"<a href='tel:+639277787931'>Globe:+639277787931</a><br>";
    echo"<a href='tel:+639062572652'>Globe:+639062572652</a><br>";
    echo"<a href='tel:+639268906894'>Globe:+639268906894</a><br>";
    echo"<a href='tel:+639096518584'>Smart:+639096518584</a><br></center> <hr>";

    echo"<br><b class='b'>Address: </b>".$dataInfo['address']."<br><br>";
    echo"<br><b class='b'>NOTE:</b> Extra mattress and kitchen utensils are not provided by resort.<br><br>";
      
  }?>

<br><br>
</div>
    
<div class="col-sm-8">
          <div class="w3-content" style="max-width:1200px;">
          <?php
          for ($counter=1; $counter <=12 ; $counter++)
                        echo" <img class='mySlides' src='resorts/$name/$counter.jpg' style='width:105%; border-radius: 10px; max-height: 500px; height: 500px;'>";
          ?>

          <div class="w3-row-padding w3-section">
            <?php
          for ($counter=1; $counter <=12 ; $counter++)
            echo"<div class='w3-col s2'>
              <img class='demo w3-opacity w3-hover-opacity-off' src='resorts/$name/$counter.jpg' style='border-radius: 10px; height: 70px; width:130%; max-width: 120px; padding: 5px; curser: pointer;'' onclick='currentDiv($counter)'>
                  </div>";?>
        </div>
          <i>PHOTO DISCLAIMER: The photo you may see is not the actual condition of the place it might effect the color, style, quality, etc, We better suggest occular visitation for the consultation of the place/location therefore we meet our expectations. Thank you so much!</i>
        </div>
            
  </div>
</div>
    </div>
  </div>


<?php
}//end of check if data has been sent or clicked
else{
  header("location: index.php");  
  exit();
}

mysqli_close($dbc);

?>
<br><br>
<?php
  include_once("footer.php");
  ob_end_flush();
?>

<script src="scripts/resort.js"></script>
</body>
</html>