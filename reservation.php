<?php
if(!empty($_POST['submit'])){
  function clean($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
  }

  $cname    = clean($_POST['name']);
  $contact = clean($_POST['contact']);
  $pax     = clean($_POST['pax']);
  $date    = clean($_POST['date']);
  $concern = clean($_POST['concern']);

    $query = "INSERT INTO reservation (name,contact,pax,pick,date,concern) VALUES ('$cname','$contact','$pax','$name','$date','$concern')";

    if (mysqli_query($dbc, $query))
        echo"<script> success(); </script>";
    else
        echo "Error: " . $query . "<br>" . mysqli_error($dbc);


}// end of when click
?>

<form action="resort.php?id=<?php echo $value ?>" method="POST">
<div>
  <center><button onclick="document.getElementById('reservation').style.display='block'" class="btn-primary btn-lg">Reserve Now!</button></center>

  <div id="reservation" class="w3-modal">
    <div class="w3-modal-content w3-animate-top w3-card-8 reservationContent">
      <header class="w3-container w3-indigo"> 
        <div align="right"><span onclick="document.getElementById('reservation').style.display='none'" 
        class="w3-closebtn" style="cursor: pointer;">&times;</span>
        </div>
        <h2 class="modal-title" align="center" style="color:orange">Send us your details!</h2><br>
      </header>
        <div class="modal-body modBody" align="left">
       <div class="row">
         <div class="col-sm-5"><h3>Your Name:</h3></div>
         <div class="col-sm-6"><input type="text" placeholder="Fullname" class="form-control reserveForm" name="name" required></div>           
       </div><br>

        <div class="row">
         <div class="col-sm-5"><h3>Mobile number:</h3></div>
         <div class="col-sm-6"><input type="tel" placeholder="Smart/TM/Globe" class="form-control reserveForm" name="contact" required></div>   
       </div><br>

       <div class="row">
         <div class="col-sm-5"><h3>Chosen resort:</h3></div>
         <div class="col-sm-6"><input type="text" placeholder="Resort name" class="form-control reserveForm" value="<?php echo $name?>" disabled required></div>
       </div><br>

       <div class="row">
         <div class="col-sm-5"><h3>Number of guest:</h3></div>
         <div class="col-sm-6"><input type="tel" placeholder="e.g 40-50" class="form-control reserveForm" name="pax" required></div>   
       </div><br>

       <div class="row">
         <div class="col-sm-5"><h3>Prepared date:</h3></div>
         <div class="col-sm-6"><input type="date" class="form-control reserveForm" name="date" required></div>
       </div><br>

      <div class="row">
         <div class="col-sm-5"><h3>Other concern:</h3></div>
         <div class="col-sm-6"><textarea class="form-control reserveForm" name="concern" maxlength="255"></textarea></div>
      </div><br><br>
      <div class="row">
      <div class="col-sm-12" align="right">
          <input class="w3-container btn btn-warning" type="submit" value="Send!" data-dismiss="modal" name="submit">
      </div>
        <br><br>
      </div>
      </div>
    </div>
  </div>
</div>
</form>