<div>
<div class="Header">
  <center class="banner"><img class="img-responsive" src="icon/banner.png"></center>
</div>
<br>
<nav class="navbar">
  <div class="container-fluid size">
	<div class="navbar-header">
							
							<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#clps" aria-expanded="false">
							<h4><img src="icon/menu.png" class="sIcon">&nbsp;&nbsp;Menu</h4>
							</button>


	</div>
	<div class="collapse navbar-collapse" id="clps">
	<ul class="nav navbar-nav">
	    <li><a href="home.php"><img src="icon/home.png" class="sIcon">&emsp;Home</a></li>
	    <li><a href="index.php"><img src="icon/resort.png" class="sIcon">&emsp;Resorts</a></li>
	    <li><a data-toggle="modal" data-target="#myModal"><img src="icon/book.png" class="sIcon">&emsp;How to book</a>

						<!-- Modal -->
					<div id="myModal" data-keyboard="false" data-backdrop="static" class="modal fade">
					  <div class="modal-dialog modal-lg modSize">
					  		<div class="row">
							<div class="col-sm-12">
					    <!-- Modal content-->
					    <div class="modal-content modContent">
					      <div class="modal-header modHeader">
					        <button type="button" class="close" data-dismiss="modal">&times;</button>
					        <h2 class="modal-title">Great! This is our guidelines to get reserve.</h2>
					      </div>
					      <div class="modal-body modBody" align="left">
					       <ul> 
					        <li><h3>Choose your Resort</h3></li>
					        <li><h3>Contact us to the corresponding number in resort details</h3></li>
					        <li><h3>Negotiate; (Talk about the downpayment process)</h3></li>
					        <li><h3>Deal</h3></li>
					        <div align="right"><h5>(Or just simply click the <i>Reserve Now</i> button and send us your details.)</h5></div>
					       </ul>					  
					      </div>
					      <div class="modal-footer modFooter">
					        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					      </div>
					    </div>
					    	</div><!-- col -->
							</div><!-- row -->		  
					  </div>
					</div>
	    </li>
	    <li><a href="about.php"><img src="icon/about.png" class="sIcon">&emsp;About us</a></li>
	</ul>
	  
	<ul class="nav navbar-nav navbar-right">
	    <li><a href="advertise.php"><img src="icon/owner.png" class="sIcon">&emsp;Submit your resort!</a></li>
	</ul>
	</div>
  </div>
</nav>
</div>


