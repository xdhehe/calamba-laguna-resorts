<!DOCTYPE html>
<html>
<head>
	<title>Advertise with us</title>
	 <meta charset="utf-8">
  	 <meta name="description" content="Calamba Laguna Resorts">
 	 <meta name="keywords" content="Affordable,Cheap,Pansol,Calamba,Laguna,Resorts,Resort">
 	 <meta name="viewport" content="width=device-width, initial-scale=1">

	  <link rel="stylesheet" type="text/css" href="styles/bootstrap.css">
  	  <link rel="stylesheet" type="text/css" href="styles/style.css">
      <link rel="stylesheet" href="styles/w3css.css">
      <script src="styles/jquery.min.js"></script>
	  <script src="styles/bootstrap.min.js"></script>
	  <script type="text/javascript" src="sweet-alert.js"></script>
</head>
<body>
 <?php
      require_once("header.php");
 ?>
<div class="container">
<div class="row">
	<div class="col-sm-12"><h1  class="h">Own a resort? Post your resort now!</h1>
	<h5>Here our some guidelines in order to be part of our industry.</h5>
	</div></div>
<br>
<hr style="border: 1px solid black; border-style: dashed; border-color: #666699;">
<div class="row">
	
	<div class="col-sm-12"><h2 class="h">Steps to do that:</h2><br><ul><h4>
		<li>Register with us <h6>(<i>click the register button bellow</i>).</h6></li><br>
		<li>Fill up the registration form.</li><br>
		<li>Afer you register, you may now Log-in and submit your resort!</li><br>
		<li>After you submit your resort we will review it first and contact you before we post it.</li><br></ul></h4>
		<h6>And that's it, as easy as that! Your Resort is on our website now.</h6>
		<br>

		<hr style="border: 1px solid black; border-style: dashed; border-color: #666699;">
		

		<h1 class="h"><b>Note: This is free!</b></h1>
		<h4><br>What are you waiting for? Submit your Resort now! Maximize your reservation, and be part of our growing industry.</h4>
		<br><br>

		<a href="owner/login.php" class="btn btn-default btn-lg">Login</a>
		 &emsp;|&emsp;
		<a href="owner/register.php" class="btn btn-default btn-lg">Register</a>

	</div>
</div>



	</div>
<?php
	require_once("footer.php");
?>

</body>
</html>

<?php
	if(!empty($_GET['success'])){ 
?>	
		<script> swal("Register Successfully!"); </script>
<?php  
}

if(isset($_POST['submit'])){


require_once("admin/connection.php");

function clean($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
return $data;
}

$name        = clean($_POST['name']);
$resortname  = clean($_POST['resortName']);
$contact     = clean($_POST['contact']);
$address     = clean($_POST['address']);
$question    = clean($_POST['question']);
if(!empty($_POST['email']))
	$email   = clean($_POST['email']);


$queryInfo = "INSERT INTO owner (name,resortname,contact,address,email,question) VALUES (?,?,?,?,?,?)";

$stmtInfo = mysqli_prepare($dbc, $queryInfo);

if (!$stmtInfo) die('mysqli error: '.mysqli_error($dbc));

mysqli_stmt_bind_param($stmtInfo, "ssssss", $name, $resortname, $contact, $address,$email,$question);
mysqli_stmt_execute($stmtInfo);

$affected_rows  = $stmtInfo->affected_rows;
if($affected_rows == 1){
	echo"<script> success(); </script>";
}
else{
	echo 'Error Occurred<br />';
	echo mysqli_error($dbc);
}

mysqli_stmt_close($stmtInfo);
mysqli_close($dbc);

}//end of when submit is click
?>

