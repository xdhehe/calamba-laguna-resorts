<!DOCTYPE html>
<html lang="en">
<head>
  <title>Home</title>
  <meta charset="utf-8">
  <meta name="description" content="Calamba Laguna Resorts">
  <meta name="keywords" content="Affordable,Cheap,Pansol,Calamba,Laguna,Resorts,Resort">
  <meta name="viewport" content="width=device-width, initial-scale=1">
 
  <link rel="stylesheet" type="text/css" href="styles/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="styles/style.css">
  <link rel="stylesheet" href="styles/w3css.css">
  <script src="scripts/featured.js"></script>
  <script src="styles/jquery.min.js"></script>
  <script src="styles/bootstrap.min.js"></script>
</head>
<body>
      <?php
      require_once("header.php");
      ?>

<div class="bodyLayout">
<div class="container">    
  <div class="row">
    <div class="col-sm-12">
	<div class="panel-header head"><img src="icon/camera.png" class="bIcons">&emsp;Featured Photo</div>
  <div class="w3-content w3-section">
  <div class="panel-body">
  
  <a href="resort.php?id=17" target="_blank"><img class="mySlides w3-animate-fading" src="featured/1.jpg" width=100% style="border: 2px solid black; border-color: orange; max-height: 500px;"></a>

  <a href="resort.php?id=21" target="_blank"><img class="mySlides w3-animate-zoom" src="featured/2.jpg" width=100% style="border: 2px solid black; border-color: orange; max-height: 500px;"></a>

  <a href="resort.php?id=18" target="_blank"><img class="mySlides w3-animate-fading" src="featured/3.jpg" width=100% style="border: 2px solid black; border-color: orange; max-height: 500px;"></a>

  <a href="resort.php?id=19" target="_blank"><img class="mySlides w3-animate-right" src="featured/4.jpg" width=100% style="border: 2px solid black; border-color: orange; max-height: 500px;"></a>

  <a href="resort.php?id=17" target="_blank"><img class="mySlides w3-animate-fading" src="featured/5.jpg" width=100% style="border: 2px solid black; border-color: orange; max-height: 500px;"></a>


  <a href="resort.php?id=15" target="_blank"><img class="mySlides w3-animate-opacity" src="featured/6.jpg" width=100% style="border: 2px solid black; border-color: orange; max-height: 500px;"></a>
  </div>
  </div>

	</div><!-- end of col sm12 -->
	</div><!-- end of row-->
	</div><!-- end of container -->
<div class="container">    
  <div class="row">
    <div class="col-sm-7">
        <div class="panel-body"><div class="textQ"><h1>Experience the summer together with your "Family". Celebrate
								your memorable wedding receptions. Hangout and bonding with your
								"Barkada". Birthday parties, or any kind of occasions that you
								want! Come and create a moment with us.
								What are you waiting for? Inquire now!</h1></div>
                <br><br><br>
                <div class="textQ"><h1>Relaxation place? Occasional locations?
                Worry no more, come and feel the beauty
                of Calamba Laguna private pool resorts.
                You're guide in every occasions that you
                preparing for.</h1></div>
          </div>        
    </div>
    <div class="col-sm-5"> 
    <br><br>

        <div class="row fbRow" align="center">
        <div class="col-sm-8 fbT">
        <img src="icon/owner.png" class="sIcon">&emsp;<b class="b1">Send us your feedback!</b> <b class="b1">And Like us on Facebook</b>
        </div>
        <div class="col-sm-4">
        <a href="http://www.facebook.com/calambalagunaresorts"><img src="icon/fb.png" class="fb"></a>  
        </div>
        </div>

        <br><br>
        <div class="panel-body motto"><img src="icon/motto.png" width="100%" height="80%"></div><br>
    </div>
  </div> <!-- end of row --> 
</div> <!-- end of container -->
<br><br>
</div><!-- end of body lagyou-->

      <?php
      require_once("footer.php");
      ?>

<script>
featured();
</script>
</body>
</html>