<!DOCTYPE html>
<html lang="en">
<head>
  <title>Resorts</title>
  <meta charset="utf-8">
  <meta name="description" content="Calamba Laguna Resorts">
  <meta name="keywords" content="Affordable,Cheap,Pansol,Calamba,Laguna,Resorts,Resort">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="refresh" content="60">

  <link rel="stylesheet" type="text/css" href="styles/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="styles/style.css">
  <link rel="stylesheet" type="text/css" href="styles/indexstyle.css">
  <link rel="stylesheet" href="styles/w3css.css">

  <script src="styles/jquery.min.js"></script>
  <script src="styles/bootstrap.min.js"></script>
  
  <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
  (adsbygoogle = window.adsbygoogle || []).push({
    google_ad_client: "ca-pub-5130765777303378",
    enable_page_level_ads: true
  });
</script>

</head>
<body>
<?php
  require_once("header.php");
  require_once("admin/connection.php");
?>
<?php   
  $queryCount = "SELECT COUNT(*) AS id FROM information";
  $responseCount = @mysqli_query($dbc, $queryCount);
  $dataCount = mysqli_fetch_row($responseCount);
  

  $count = $dataCount[0];
  $pages =  ceil($count / 7);
//FOR PAGE
  if(!empty($_GET['page'])){ //if click in pages
    $activePage = $_GET['page'];
    if($activePage>=1)
      $offset = ($activePage * 7) - 6; //set value of id where to start
  }
  else
    $offset = 1;

    $cond = $offset -1;
    $queryInfo = "SELECT id,name,pax FROM information LIMIT 7 OFFSET $cond";
    $queryPrice ="SELECT oDay12Day,oDay12Night,oDay24, pEnd12Day,pEnd12Night,pEnd24
    FROM price  LIMIT 7 OFFSET $cond";
?>


<div class="container">
<div class="row">
<div class="col-sm-6" align="center"><br>
<a class="btn btn-warning sort" href='resortssort.php?sort=asc'>Sort form: A-Z</a>
<a class="btn btn-warning sort" href='resortssort.php?sort=des'>Sort from: Z-A</a>
</div><!-- col -->
<div class="col-sm-6" align="center"><br>
<form name="formSearch" method="POST" action="resortssearch.php">
    <input type="text" class="inputS" placeholder="Resort name" name="search" required>
    <input type="submit" class="btn btn-default" name="submitSearch" value="Search">
</form>
</div><!-- col -->
</div><!-- row -->
<br><hr>

<div class="row rowBody">
<div class="col-sm-5 colBody" align="center">
<form name="formBudget" method="POST" action="resortbudget.php">
  Price Range:&emsp;
  From: <input type="number" class="inputP" name="from" placeholder="₱3000" min="3000" max="99999" required>&nbsp;
  To:   <input type="number" class="inputP" name="to" placeholder="₱99999"  min="3000" max="99999" required>&emsp;
</div><!-- col -->
<div class="col-sm-5 colBody" align="center">
  Sort by budget:
    <select name="order" class="select" required>
      <option value="cheap">Cheapest to Most expensive</option>
      <option value="costly">Most expensive to Chepeast</option>
    </select>           
</div><!-- col -->
<div class="col-sm-2 colBody" align="center">
    <input type="submit" class="btn btn-warning" name="submitBudget">
</div><!-- col -->
</form>
</div><!-- row -->


<hr style="border-width: 10px; border-color: orange;">


<?php
  $responseInfo  = @mysqli_query($dbc, $queryInfo);
  $responsePrice = @mysqli_query($dbc, $queryPrice);

  if($responseInfo && $responsePrice){

    while($dataInfo = mysqli_fetch_array($responseInfo)){
      $dataPrice = mysqli_fetch_array($responsePrice);

      // echo"COUNT :".$dataInfo['id']. '<br><br>';
      $id  = $dataInfo['id'];
      $name = $dataInfo['name'];
      ?>
      <div class="row">
      <div class="col-sm-3" align="center">

      <?php
      echo"<a href='resort.php?id=$id' target='_blank'>";
      echo"<img src='resorts/$name/1.jpg' style='height:150px; width:auto; max-width:190px; border-radius: 5px;'>";      
      ?>

      </div><div class="col-sm-3" align="center">

      <?php
      echo '<br><b>'.$name.'</b><br><br>';
      echo"Pax can accommodate: ".$dataInfo['pax']. '<br><br>';
      ?>

      </div><div class="col-sm-3 padTop" align="center">

      <?php
      echo"Daytime: ₱".$dataPrice['oDay12Day']." - ₱".$dataPrice['pEnd12Day']."<br>";
      echo"Overnight: ₱".$dataPrice['oDay12Night']." - ₱".$dataPrice['pEnd12Night']."<br>";
      echo"24hrs: ₱".$dataPrice['oDay24']." - ₱".$dataPrice['pEnd24']."</a><br><br>"; 
      ?>

      </div><div class="col-sm-3 padTop" align="center">      

      <?php
      echo"<a href='tel:+639277787931'><img src='icon/phone.png' class='telIcon'>&emsp;Globe:+639277787931</a><br>";
      echo"<a href='tel:+639062572652'><img src='icon/phone.png' class='telIcon'>&emsp;Globe:+639062572652</a><br>";
      echo"<a href='tel:+639268906894'><img src='icon/phone.png' class='telIcon'>&emsp;Globe:+639268906894</a><br>";
      echo"<a href='tel:+639096518584'><img src='icon/phone.png' class='telIcon'>&emsp;Smart:+639096518584</a><br>";
      ?>

      </div></div>
      <hr style="border-width: 5px; border-color: orange;">
      <?php
    }
  }//end of response

  if(!empty($_GET['page']))
    echo "Page:$activePage<br>";
  
  echo "<div align='right'>";
    for($counter=1;$counter<=$pages;$counter++){
      if($pages>0)
        echo"<a class='btn btn-default btn-sm' href='index.php?page=$counter'>".$counter."</a> ";
    }
  echo"</div>";
  
  mysqli_close($dbc);
?>
</div>
  <?php
    include_once("footer.php");
  ?>

</body>
</html> 