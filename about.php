<!DOCTYPE html>
<html lang="en">
<head>
  <title>About</title>
  <meta charset="utf-8">
  <meta name="description" content="Calamba Laguna Resorts">
  <meta name="keywords" content="Affordable,Cheap,Pansol,Calamba,Laguna,Resorts,Resort">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="stylesheet" type="text/css" href="styles/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="styles/style.css">
  <link rel="stylesheet" href="styles/w3css.css">
  <script src="styles/jquery.min.js"></script>
  <script src="styles/bootstrap.min.js"></script>
</head>
<body>
      <?php
      require_once("header.php");
      ?>
<div class="Body">
<div class="container">    
  <div class="row">
    <div class="col-sm-12">
        <div class="panel-header head"><img src="icon/about.png" class="bIcons">
        About us:</div><hr>
        <div class="panel-body"><p class="p">Establish in year 2017, a newly reliable legitimate online resort advertisement, that humbly develop a site that provides the needs and a solution that may help you to guide as also as to seek a quality resorts. Has a goal to expand and to disseminate a resort industry here in Laguna. We're not the first one who develop an information system, but we assure the good and quality service that you are looking for.</p>
        </div>
    </div>
  </div>

  <div class="row">
      <div class="col-sm-12">
          <div class="panel-header head"><img src="icon/objectives.png" class="bIcons">
          Objectives:</div><hr>
          <div class="panel-body">
            <ul>
              <li class="p">To help and guide you to find a relaxation place that may suit in your preference.</li><br>
              <li class="p">To make a reliable source of information that worry no more to ocular or to visit the place.</li><br>
              <li class="p">To give you a quality service.</li><br>
              <li class="p">To give you a place to bond with your love ones.</li>
            </ul>
          </div>
    </div>
  </div>

  <div class="row">
    <div class="col-sm-12">
        <div class="panel-header head"><img src="icon/place.png" class="bIcons">
        Place:</div><hr>
        <div class="panel-body"><p class="p">Calamba Laguna was one of the very famous and popular province in Luzon. There are hundreds of resorts and private pools mainly concentrated here. Laguna known as the resort capital of the Philippines.
        </p>
        </div>
    </div>
  </div>
<br><br>
  <div class="row" align="right">
    <div class="col-sm-12">
        <div class="panel-body"><p>Developers @email:<br>
        <a href="mailto:garcia.johnromil@gmail.com?Subject=Hello" target="_top">garcia.johnromil@gmail.com</a>
        <a href="mailto:dp.boredstudent@gmail.com?Subject=Hello" target="_top">dp.boredstudent@gmail.com</a><br>
        
        </p>
        </div>
    </div>
  </div>


</div> <!--container-->
</div> <!--Body-->
  <?php
    include_once("footer.php");
  ?>
  </footer>
</body>
</html>
