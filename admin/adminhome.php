<?php
ob_start();
session_start();
?>

<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="styles/adminhomestyle.css">
	<link rel="stylesheet" type="text/css" href="styles/bootstrap.css">
</head>
<body  background="pics/adminbg.png"  style="height: 100% background-position: center; background-repeat: no-repeat; background-size: cover;" > <!---->
<?php
if(isset($_SESSION['username']) && isset($_SESSION['password'])){
?>
	<div align="right" class="marg">
	<a class="btn btn-warning" href='logout.php'><u>Log-out</u></a>
	</div><!-- container -->
	<div class="container bodyLayout">
	<div class="row">
					<div class="col-sm-6 pad">
					<div class="panel-body">
						<li class="liT">View/Update Resort(s)</li>
							<ul type='disc' class="ul">
								<li class="li"><a href='viewinfo.php'><b>Information about resort(s)</b></a></li>
								<li class="li"><a href='viewpool.php'><b>Pool information about resort(s)</b></a></li>
								<li class="li"><a href='viewpicture.php'><b>Pictures of resort(s)</b></a></li>
								<li class="li"><a href='viewprice.php'><b>Price information about resort(s)</b></a></li>
							</ul>	
					</div><!-- panel body -->
					</div><!-- end col 1 -->	
					<div class="col-sm-6 pad">
					<div class="panel-body">
						<li class="liT">Add/Delete</li>
							<ul type='disc' class="ul">
								<li class="li"><a href='delete.php'><b>Delete resort(s)</b></a></li>
								<li class="li"><a href='add.php'><b>Add another resort</b></a></li>
							</ul>
						<li class="liT">Others</li>
							<ul type='disc' class="ul">
								<li class="li"><a href='owner.php'><b>Pending Resort(s)</b></a></li>
								<li class="li"><a href='reservation.php'><b>Reservation(s)</b></a></li>
								<li class="li"><a href='backup.php'><b>Backup SQL</b></a></li>
							</ul>
					</div><!-- panel body -->
					</div><!-- end col 2 -->
	</div><!-- end row 2-->
	</div><!-- end container -->

<?php
}
else{
	header("Location:login.php");
	exit();
}
ob_end_flush();
?>
</body>
</html>