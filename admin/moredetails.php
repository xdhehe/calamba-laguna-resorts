<?php
ob_start();
session_start();
?>

<!DOCTYPE html>
<html>
<head>
	<title>More Details</title>
	<link rel="stylesheet" type="text/css" href="styles/resortstyle.css">

<?php
	require_once("menunav.php");
?>
<script src="menu.js"></script>
</head>
<body>

<div class="container">
<div class="row">
<div class="col-sm-12" align="center">
	<h1 class="h">Pending resort</h1>
	<hr>
</div><!-- col -->
</div><!-- row -->

<?php
if(isset($_SESSION['username']) && isset($_SESSION['password'])){
	require_once("connection.php");
	$id = $_GET['id'];	

	$queryInfo =  "SELECT * FROM pendinginformation WHERE id = '$id'";
	$queryPool  = "SELECT * FROM pendingpool        WHERE poolId = '$id'";
	$queryPrice = "SELECT * FROM pendingprice       WHERE priceId = '$id'";

	$responseInfo = @mysqli_query($dbc,$queryInfo);
	$responsePool = @mysqli_query($dbc,$queryPool);
	$responsePrice= @mysqli_query($dbc,$queryPrice);

	
	$dataInfo  = mysqli_fetch_array($responseInfo);
	$dataPool  = mysqli_fetch_array($responsePool);
	$dataPrice = mysqli_fetch_array($responsePrice);

	$owner = $dataInfo['ownername'];

	$queryOwner = "SELECT * FROM owner WHERE username = '$owner'";
	$responseOwner = mysqli_query($dbc,$queryOwner);
	$dataOwner = mysqli_fetch_array($responseOwner);

	$name = $dataInfo['name'];
?>
<div class="row row1" >
<div class="col-sm-6">

<?php
//BASIC INFO ABT OWNER
	echo"<b>Full Name:</b>&emsp;"  .$dataOwner['fullname']."<br>";
	echo"<b>Contact no.:</b>&emsp;".$dataOwner['contact']."<br>";
	echo"<b>@Email:</b>&emsp;"     .$dataOwner['email']."<br>";
?>

</div><!-- col -->
<div class="col-sm-6">



<?php
	echo"<b>Username:</b>&emsp;"   .$dataOwner['username']."<br>";
	echo"<b>Password:</b>&emsp;"   .$dataOwner['password']."<br><br>";
?>



</div><!-- col -->
</div><!-- row -->
<hr>
<div class="row row" >
<div class="col-sm-4 row1 resortD">
<h2 class="b">Resort details</h2>
<br>
<?php
//BASIC INFO ABT RSRT
	echo"Resort name:&emsp;"       .$name."<br>";
	echo"Pax can accomodate:&emsp;".$dataInfo['pax']."<br>";
	echo"Bedrooms:&emsp;"          .$dataInfo['bedroom']."<br>";
	echo"Aircon:&emsp;"            .$dataInfo['aircon']."<br>";
	echo"Sleeping Capacity:&emsp;" .$dataInfo['sleep']."<br>";
	echo"Facilities:&emsp;"        .$dataInfo['facilities']."<br>";
	echo"Amenities:&emsp;"         .$dataInfo['amenities'] ."<br><br>";

//INFO PRICE
	echo"Peak Season<br>";
		echo"Weekdays<br>";	
			echo"12hours Day Time:&emsp;"  .$dataPrice['pDay12Day']."<br>";	
			echo"12Hours Night Time:&emsp;".$dataPrice['pDay12Night']."<br>";	
			echo"24 Hours:&emsp;"          .$dataPrice['pDay24']."<br>";	
		echo"Weekends<br>";				
			echo"12hours Day Time:&emsp;"  .$dataPrice['pEnd12Day']."<br>";	
			echo"12Hours Night Time:&emsp;".$dataPrice['pEnd12Night']."<br>";	
			echo"24 Hours:&emsp;"          .$dataPrice['pEnd24']."<br><br>";	

	echo"Off Season<br>";
		echo"Weekdays<br>";	
			echo"12hours Day Time:&emsp;"  .$dataPrice['oDay12Day']."<br>";	
			echo"12Hours Night Time:&emsp;".$dataPrice['oDay12Night']."<br>";	
			echo"24 Hours:&emsp;"          .$dataPrice['oDay24']."<br>";	
		echo"Weekends<br>";				
			echo"12hours Day Time:&emsp;"  .$dataPrice['oEnd12Day']."<br>";	
			echo"12Hours Night Time:&emsp;".$dataPrice['oEnd12Night']."<br>";	
			echo"24 Hours:&emsp;"          .$dataPrice['oEnd24']."<br><br>";	

		if(!empty($dataInfo['otherexpenses']))
			echo"Other Expenses:"          .$dataInfo['otherexpenses']."<br><br>";
//INFO POOL
//ADULT
	echo"Adult pool quantity:&emsp;"         .$dataPool['adultQuantity']."<br>";
	echo"Adult first pool feet:&emsp;"       .$dataPool['aFirstFeet']." feet<br>";
	echo"Adult first pool temperature:&emsp;".$dataPool['aFirstTemp']."<br>";
	if(!empty($dataPool['aFirstSlide']))
		echo"Adult first pool slide:&emsp;"      .$dataPool['aFirstSlide']."<br>";

		if(!empty($dataPool['aSecondFeet']))
		echo"Adult second pool feet:&emsp;"  .$dataPool['aSecondFeet']." feet<br>";
	if(!empty($dataPool['aSecondTemp']))
		echo"Adult second temperature:&emsp;".$dataPool['aSecondTemp']."<br>";
	if(!empty($dataPool['aSecondSlide']))
		echo"Adult second pool slide:&emsp;" .$dataPool['aSecondSlide']."<br>";

	if(!empty($dataPool['aThirdFeet']))
		echo"Adult Third pool feet:&emsp;"  .$dataPool['aThirdFeet']." feet<br>";
	if(!empty($dataPool['aThirdTemp']))
		echo"Adult Third temperature:&emsp;".$dataPool['aThirdTemp']."<br>";
	if(!empty($dataPool['aThirdSlide']))
		echo"Adult Third pool slide:&emsp;" .$dataPool['aThirdSlide']."<br>";
//KIDS

	if(!empty($dataPool['kFirstFeet']))
		echo"Kid First pool feet:&emsp;"  .$dataPool['kFirstFeet']." feet<br>";
	if(!empty($dataPool['kFirstTemp']))
		echo"Kid First temperature:&emsp;".$dataPool['kFirstTemp']."<br>";
	if(!empty($dataPool['kFirstSlide']))
		echo"Kid First pool slide:&emsp;" .$dataPool['kFirstSlide']."<br>";

	if(!empty($dataPool['kSecondFeet']))
		echo"Kid Second pool feet:&emsp;"  .$dataPool['kSecondFeet']." feet<br>";
	if(!empty($dataPool['kSecondTemp']))
		echo"Kid Second temperature:&emsp;".$dataPool['kSecondTemp']."<br>";
	if(!empty($dataPool['kSecondSlide']))
		echo"Kid Second pool slide:&emsp;" .$dataPool['kSecondSlide']."<br>";

	if(!empty($dataPool['kThirdFeet']))
		echo"Kid Third pool feet:&emsp;"  .$dataPool['kThirdFeet']." feet<br>";
	if(!empty($dataPool['kThirdTemp']))
		echo"Kid Third temperature:&emsp;".$dataPool['kThirdTemp']."<br>";
	if(!empty($dataPool['kThirdSlide']))
		echo"Kid Third pool slide:&emsp;" .$dataPool['kThirdSlide']."<br>";
?>
</div><!-- col -->
<div class="col-sm-6">
<?php
	for($counter=1;$counter<=4;$counter++)
		echo"<img src='../pending/$name/$counter.jpg' style='height:150px; width:auto; max-width:200px; border-radius: 10px; padding: 5px;'>";

	for($counter=5;$counter<=8;$counter++)
		echo"<img src='../pending/$name/$counter.jpg' style='height:150px; width:auto; max-width:200px; border-radius: 10px; padding: 5px;'>";

	for($counter=9;$counter<=12;$counter++)
		echo"<img src='../pending/$name/$counter.jpg' style='height:150px; width:auto; max-width:200px; border-radius: 10px; padding: 5px;'>";
	
	mysqli_close($dbc);// Close connection to the database
?>
</div><!-- col -->
</div><!-- row -->



<?php
}else{
	header("Location:login.php");
	exit();
}
ob_end_flush();
?>



</div><!-- container -->
</body>
</html>