<?php
ob_start();
session_start();
?>


<!DOCTYPE html>
<html>
<head>
	<title>Reservations</title>
   	<link rel="stylesheet" type="text/css" href="styles/adminview.css">
</head>
<?php
	require_once("menunav.php");
?>
<script src="menu.js"></script>
</html>

<?php
if(isset($_SESSION['username']) && isset($_SESSION['password'])){
	require_once('connection.php');

	if(!empty($_GET['done'])){
		$done = $_GET['done'];
		$queryDeleteReserve = "DELETE FROM reservation WHERE id = '$done'";

		if (mysqli_query($dbc, $queryDeleteReserve))
			    echo "Finish!";
	}


	$queryReservation = "SELECT id,name,contact,pick,pax,date,concern,time FROM reservation";
	$responseReserve  = @mysqli_query($dbc, $queryReservation);
	if($responseReserve){

		echo"<br><div class='title' align='center'>Costumer's Reservation</div><br>";
		echo'<table align="left" cellspacing="2" cellpadding="6" class="table">
		<tr>
			<th class="th"><b>Customer name</b></th>
			<th class="th"><b>Contact</b></th>
			<th class="th"><b>Chosen</b></th>
			<th class="th"><b>No.of Guest</b></th>
			<th class="th"><b>Date prepared</b></th>
			<th class="th"><b>Other Concern</b></th>
			<th class="th"><b>Time Added</b></th>
			<th class="th"><b>Status</b></th>
		</tr>';

		// mysqli_fetch_array will return a row of data from the query
		// until no further data is available
		while($dataReserve = mysqli_fetch_array($responseReserve)){
			$id = $dataReserve['id'];
		echo '<tr class="tr">
			<td class="td">'.$dataReserve['name'].'</td>
			<td class="td">'.$dataReserve['contact'].'</td>
			<td class="td">'.$dataReserve['pick'].'</td>
			<td class="td">'.$dataReserve['pax'].'</td>
			<td class="td">'.$dataReserve['date'].'</td>
			<td class="td">'.$dataReserve['concern'].'</td>
			<td class="td">'.$dataReserve['time'].'</td>
			<td class="td">'."<a href='reservation.php?done=$id'>Done</a>".'</td>
		</tr>';
	}
	echo '</table>';
	} else {
		echo "Couldn't issue database query<br />";
		echo mysqli_error($dbc);
	}
	mysqli_close($dbc);// Close connection to the database
}
else{
	header("Location:login.php");
	exit();
}
ob_end_flush();
?>