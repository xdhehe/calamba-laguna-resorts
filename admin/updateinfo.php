<?php
ob_start();
session_start();
?>

<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="styles/adminview.css">
</head>
<?php
	require_once("menunav.php");
?>
<script src="menu.js"></script>
</html>

<?php 
require_once("connection.php");

if(isset($_SESSION['username']) && isset($_SESSION['password'])){

if(!empty($_GET['id'])){
	$id = $_GET['id'];
	$_SESSION['id'] =$id;
}
else
	$id = $_SESSION['id'];

$queryInfoView = "SELECT name,address,ctname,contact,email, pax,bedroom,aircon,sleep,facilities,amenities,otherexpenses,time FROM information where id = '$id'";
$responseInfoView  = @mysqli_query($dbc, $queryInfoView);
$dataInfoView = mysqli_fetch_array($responseInfoView);	

		$name = $dataInfoView['name'];
	if(isset($_POST["update"])){
		$updatedName = trim($_POST['updatedName']);
		
		$old = "../resorts/$name";
		$new = "../resorts/$updatedName";
		
		if(rename("$old","$new"))
			echo"Successufully renamed";

		$address    = trim($_POST['address']);

		$ctname  = (!empty($_POST['ctname'])) ? $ctname = trim($_POST['ctname']):null;
		$contact = (!empty($_POST['contact'])) ? $email = trim($_POST['contact']):null;
		$email   = (!empty($_POST['email'])) ? $email = trim($_POST['email']):null;

		$pax        = trim($_POST['pax']);
		$bedroom    = trim($_POST['bedroom']);
		$aircon     = trim($_POST['aircon']);
		$sleep      = trim($_POST['sleep']);
		$facilities = trim($_POST['facilities']);
		$amenities  = trim($_POST['amenities']);

		$otherExpenses = (!empty($_POST['otherexpenses'])) ? $otherExpenses = trim($_POST['otherexpenses']):null;


$qPrice = "UPDATE price SET priceName='$updatedName' WHERE priceId='$id'";
$qPool  = "UPDATE pool SET poolName='$updatedName' WHERE poolId='$id'";

if ((mysqli_query($dbc, $qPrice)) && (mysqli_query($dbc, $qPool))){
			
	$qInfo = "UPDATE information SET name='$updatedName', address='$address', ctname='$ctname', contact='$contact', email='$email',pax='$pax', bedroom='$bedroom', aircon='$aircon', sleep='$sleep', facilities='$facilities', amenities= '$amenities', otherexpenses= '$otherExpenses' WHERE id ='$id'";

	$qCtName = checkingNone('ctname');
	$qContact = checkingNone('contact');
	$qOtherExpenses = checkingNone('otherexpenses');
	$qAircon = checkingZero('aircon');

		if (mysqli_query($dbc, $qInfo) && mysqli_query($dbc, $qCtName) && mysqli_query($dbc, $qContact) && mysqli_query($dbc, $qOtherExpenses) && mysqli_query($dbc, $qAircon)){
	    	echo "Updated successfully<br>";
	    	header("location:viewinfo.php");
		}
		else 
		    echo mysqli_error($dbc);
}//end of updating constraints
else
	echo mysqli_error($dbc);
}// end of updating function
mysqli_close($dbc);// Close connection to the database

}//end of checking if login
else{
	header("Location:login.php");
	exit();
}
function checkingZero($string){
		$result = "UPDATE information SET $string= null WHERE $string = '0'";
		return $result;
}

function checkingNone($string){
		$result = "UPDATE information SET $string= null WHERE $string =''";
		return $result;
}
ob_end_flush();
?>

<!DOCTYPE html>
<html>
<head>
<title>Update Resort Information</title>
  	 <link rel="stylesheet" type="text/css" href="styles/update.css">
  	 <link rel="stylesheet" type="text/css" href="styles/bootstrap.css">
</head>

<body class="body">
<div class="container">
<form action="updateinfo.php" method="post" class="form">
<br>
<div class="container">
<div align="center" class="title">Update your resort basic information</div>    
  <div class="row">
  <hr>
    <div class="col-sm-5 p">
				You're currently updating resort: <?php echo $dataInfoView['name']; ?></div>
  </div>
</div>
<hr>
<div class="container">    
  <div class="row">
    <div class="col-sm-3 p">
				Resort name:&nbsp;</div>
	<div class="col-sm-6 p">
				<input class="form-control inputT" type='text' name='updatedName' required maxlength='50' value="<?php echo $name ?>">
	</div>
  </div>

  <div class="row">
    <div class="col-sm-3 p">
				Complete resort address:&nbsp;</div>
	<div class="col-sm-6 p">
				<input class="form-control inputT" type='text' name='address' required maxlength='100' value="<?php echo $dataInfoView['address']; ?>">
	</div>
  </div>

  <div class="row">
    <div class="col-sm-3 p">
				Care taker name:&nbsp;</div>
	<div class="col-sm-6 p">
				<input class="form-control inputT" type='text' name='ctname' maxlength='50' value="<?php echo $dataInfoView['ctname']; ?>" '>	
	</div>
  </div>

  <div class="row">
    <div class="col-sm-3 p">
				Contact number:&nbsp;</div>
	<div class="col-sm-6 p">
				<input class="form-control inputT" type='text' name='contact' maxlength='20' value="<?php echo $dataInfoView['contact']; ?>"'>
	</div>			
  </div>

  <div class="row">
    <div class="col-sm-3 p">
			@Email:&nbsp;</div>
	<div class="col-sm-6 p">
			<input class="form-control inputT" type='email' name='email' maxlength='50' value="<?php echo $dataInfoView['email']; ?>"'>
  	</div>
  </div>  
</div>
<hr>

<div class="container">
	<div class="row">
    	<div class="col-sm-3 p">
			Pax can accomodate:</div>
		<div class="col-sm-3 p">	
			<input class="form-control inputT" type='text' name='pax' required maxlength='10' value="<?php echo $dataInfoView['pax']; ?>"">
		</div>
	</div>

	<div class="row">
    	<div class="col-sm-3 p">
			Bedrooms:</div>
		<div class="col-sm-2 p">
			<input class="form-control inputT" type="number" name="bedroom" min="1" max="100"  required value="<?php echo $dataInfoView['bedroom']; ?>"></div>
		<div class="col-sm-1 p">
				AirCon:</div>
		<div class="col-sm-2 p">
				<input class="form-control inputT" type="number" name="aircon" min="0" max="100" required value="<?php echo $dataInfoView['aircon']; ?>">
		</div>
	</div>

	<div class="row">
    	<div class="col-sm-3 p">
			Sleeping capacity:</div>
		<div class="col-sm-3 p">
			<input class="form-control inputT" type='text' name='sleep' required maxlength='10' value="<?php echo $dataInfoView['sleep']; ?>">
	</div>
  </div>
</div>
<hr>

<div class="container">
	<div class="row">
    	<div class="col-sm-3 p">
			Facilities:</div>
		<div class="col-sm-6 p">
			<input class="form-control inputT" type='text' name='facilities'  required value="<?php echo $dataInfoView['facilities']; ?>">
     </div>
  </div>

  	<div class="row">
    	<div class="col-sm-3 p">
			Amenities:</div>
		<div class="col-sm-6 p">
			<input class="form-control inputT" type='text' name='amenities' required value="<?php echo $dataInfoView['amenities']; ?>">	
	 </div>
	</div>

<div class="row">
    	<div class="col-sm-3 p">
			Other expenses:</div>
		<div class="col-sm-6 p">
			<input class="form-control inputT" type='text' name='otherexpenses' value="<?php echo $dataInfoView['otherexpenses']; ?>"></div>	
</div>
</div>
<hr>
<div align="right"><input type="submit" name="update" value="Update"></div>
</div>
</form>
</div>
<br><br>
</body>
</html>