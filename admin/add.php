<?php
ob_start();
session_start();
?>

<!DOCTYPE html>
<html>
<head>
 	<link rel="stylesheet" type="text/css" href="styles/bootstrap.css">
  	<link rel="stylesheet" type="text/css" href="styles/menustyle.css">
</head>
<?php

	require_once("menunav.php");
?>

<script>
function openNav() {
    document.getElementById("myNav").style.height = "100%";
}
	
function closeNav() {
    document.getElementById("myNav").style.height = "0%";
}

</script>
</html>

<?php
class add{
function add(){
if(isset($_SESSION['username']) && isset($_SESSION['password'])){
	if(isset($_POST['submit'])){	
		$name    = trim($_POST['name']);
		$address = trim($_POST['address']);
		$ctname  = (!empty($_POST['ctname'])) ? $ctname = trim($_POST['ctname']):null;
		$contact = (!empty($_POST['contact'])) ? $email = trim($_POST['contact']):null;
		$email   = (!empty($_POST['email'])) ? $email = trim($_POST['email']):null;

		$pax = trim($_POST['pax']);
		$bedroom = trim($_POST['bedroom']);
		$aircon   = (($_POST['aircon']) !='0') ? $aircon = trim($_POST['aircon']):null;
		$sleep = trim($_POST['sleep']);
		$facilities = trim($_POST['facilities']);
		$amenities = trim($_POST['amenities']);
		$otherExpenses = (!empty($_POST['otherexpenses'])) ? $otherExpenses = trim($_POST['otherexpenses']):null;
		
		$pDay12Day    = trim($_POST['pDay12Day']);
		$pDay12Night  = trim($_POST['pDay12Night']);
		$pDay24       = trim($_POST['pDay24']);							
		$pEnd12Day    = trim($_POST['pEnd12Day']);										
		$pEnd12Night  = trim($_POST['pEnd12Night']);				
		$pEnd24       = trim($_POST['pEnd24']);

		$oDay12Day    = trim($_POST['oDay12Day']);
		$oDay12Night  = trim($_POST['oDay12Night']);
		$oDay24       = trim($_POST['oDay24']);							
		$oEnd12Day    = trim($_POST['oEnd12Day']);										
		$oEnd12Night  = trim($_POST['oEnd12Night']);				
		$oEnd24       = trim($_POST['oEnd24']);

		$adultQuantity = trim($_POST['adultQuantity']);
		$aFirstFeet    = trim($_POST['aFirstFeet']);
		$aFirstSlide   = (empty($_POST['aFirstSlide']))? $aFirstSlide = NULL:trim($_POST['aFirstSlide']);
		$aFirstTemp    = trim($_POST['aFirstTemp']);

		$aSecondFeet   = ($_POST['aSecondFeet'] == 0) ? $aSecondFeet = NULL:trim($_POST['aSecondFeet']);
		$aSecondSlide  = (empty($_POST['aSecondSlide']))? $aSecondSlide = NULL:trim($_POST['aSecondSlide']);
		$aSecondTemp   = (empty($_POST['aSecondTemp']))? $aSecondTemp = NULL:trim($_POST['aSecondTemp']);

		$aThirdFeet    = ($_POST['aThirdFeet'] == 0) ? $aThirdFeet = NULL:trim($_POST['aThirdFeet']);
		$aThirdSlide   = (empty($_POST['aThirdSlide']))? $aThirdSlide = NULL:trim($_POST['aThirdSlide']);
		$aThirdTemp    = (empty($_POST['aThirdTemp']))? $aThirdTemp = NULL:trim($_POST['aThirdTemp']);

		$kidQuantity   = ($_POST['kidQuantity'] == 0)? $kidQuantity = NULL:trim($_POST['kidQuantity']);
		$kFirstFeet    = ($_POST['kFirstFeet'] == 0)? $kFirstFeet = NULL:trim($_POST['kFirstFeet']);
		$kFirstSlide   = (empty($_POST['kFirstSlide']))? $kFirstSlide = NULL:trim($_POST['kFirstSlide']);
		$kFirstTemp    = (empty($_POST['kFirstTemp']))? $kFirstTemp = NULL:trim($_POST['kFirstTemp']);

		$kSecondFeet   = ($_POST['kSecondFeet'] == 0)? $kSecondFeet = NULL:trim($_POST['kSecondFeet']);
		$kSecondSlide  = (empty($_POST['kSecondSlide']))? $kSecondSlide = NULL:trim($_POST['kSecondSlide']);
		$kSecondTemp   = (empty($_POST['kSecondTemp']))? $kSecondTemp = NULL:trim($_POST['kSecondTemp']);

		$kThirdFeet    = ($_POST['kThirdFeet'] == 0)? $kThirdFeet = NULL:trim($_POST['kThirdFeet']);
		$kThirdSlide   = (empty($_POST['kThirdSlide']))? $kThirdSlide = NULL:trim($_POST['kThirdSlide']);
		$kThirdTemp    = (empty($_POST['kThirdTemp']))? $kThirdTemp = NULL:trim($_POST['kThirdTemp']);


		require_once("connection.php");
//CREATING DIRECTORY
		if(mkdir("../resorts/$name"))
			echo"Directory created";

		require_once("upload.php");

//INSERTION OF INFO				
		$queryInfo = "INSERT INTO information (name,infoODay12Day,address,ctname,contact,email, pax,bedroom,aircon,sleep,facilities,amenities,otherexpenses)
		VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";

		$stmtInfo = mysqli_prepare($dbc, $queryInfo);
		if (!$stmtInfo) die('mysqli error: '.mysqli_error($dbc));
// i Integers, d Doubles, b Blobs, s Everything Else
		mysqli_stmt_bind_param($stmtInfo, "sisssssisssss", $name,$oDay12Day,$address,$ctname,$contact,$email,
			$pax,$bedroom,$aircon,$sleep,$facilities,$amenities,$otherExpenses);
		mysqli_stmt_execute($stmtInfo);

//INSERTION OF PRICE REFERECING IN INFO Name
		$queryPrice = "INSERT INTO price (priceName, pDay12Day, pDay12Night, pDay24, pEnd12Day, pEnd12Night, pEnd24, oDay12Day, oDay12Night, oDay24, oEnd12Day, oEnd12Night, oEnd24)
		VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";

		$stmtPrice = mysqli_prepare($dbc,$queryPrice);
		if (!$stmtPrice) die('mysqli error: '.mysqli_error($dbc));
		
		mysqli_stmt_bind_param($stmtPrice, "siiiiiiiiiiii",$name,
$pDay12Day,$pDay12Night,$pDay24,$pEnd12Day,$pEnd12Night,$pEnd24,
$oDay12Day,$oDay12Night,$oDay24,$oEnd12Day,$oEnd12Night,$oEnd24);

		mysqli_stmt_execute($stmtPrice);

//INSERTION OF POOLS REFERECING IN INFO NAME
		$queryPool = "INSERT INTO pool (poolName, kidQuantity,kFirstFeet,kFirstSlide,kFirstTemp, kSecondFeet,kSecondSlide,kSecondTemp, kThirdFeet,kThirdSlide,kThirdTemp,
		adultQuantity,aFirstFeet,aFirstSlide,aFirstTemp, aSecondFeet,aSecondSlide,aSecondTemp, aThirdFeet,aThirdSlide,aThirdTemp)
		VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		$stmtPool = mysqli_prepare($dbc,$queryPool);
		if(!$stmtPool) die('mysqli error' . mysqli_error($dbc));

		mysqli_stmt_bind_param($stmtPool,"sidssdssdssidssdssdss",
			$name,$kidQuantity,$kFirstFeet,$kFirstSlide,$kFirstTemp, 
			$kSecondFeet,$kSecondSlide,$kSecondTemp, $kThirdFeet,$kThirdSlide,$kThirdTemp,
$adultQuantity,$aFirstFeet,$aFirstSlide,$aFirstTemp, $aSecondFeet,$aSecondSlide,$aSecondTemp, $aThirdFeet,$aThirdSlide,$aThirdTemp);

		mysqli_stmt_execute($stmtPool);

		$affected_rows  = $stmtInfo->affected_rows;
		$affected_rows2 = $stmtPrice->affected_rows;
		$affected_rows3 = $stmtPool->affected_rows;

		if($affected_rows == 1 && $affected_rows2 ==1 && $affected_rows3 ==1){
			header("Location:viewinfo.php");
			exit();
		} else {
			echo 'Error Occurred<br />';
			echo mysqli_error($dbc);
		}

		mysqli_stmt_close($stmtInfo);
		mysqli_stmt_close($stmtPrice);
		mysqli_stmt_close($stmtPool);
		mysqli_close($dbc);

	}//end of if isset submit
}// end of checking of user and pass
else{
	header("Location:login.php");
	exit();}
}//end of function
}//end of class	

new add;
ob_end_flush();
?>

<!DOCTYPE html>
<html>
<head>
	<title>Add another resort.</title>
	<meta charset="utf-8">
  	<link rel="stylesheet" type="text/css" href="styles/update.css">
	<link rel="stylesheet" type="text/css" href="styles/bootstrap.css">
</head>
<body>
<div class="container">
	<form action='add.php' method="post" enctype="multipart/form-data">
	<div align="center" class="title">Add another resort</div>
	<hr>
	<div class="container">
		<div class="row">
			<div class="col-sm-3 p">
						Resort Name:&nbsp;</div>
			<div class="col-sm-6 p">
						<input class="form-control inputT" type='text' name='name' required maxlength='50'  placeholder='e.g. Summertime Resort'></div>
		</div>

			<div class="row">
			    <div class="col-sm-3 p">
						Complete resort address:&nbsp;</div>
					<div class="col-sm-6 p">
						<input class="form-control inputT" type='text' name='address' required maxlength='100'></div>
			</div>

			<div class="row">
			    <div class="col-sm-3 p">			
						Care taker name:&nbsp;</div>
				<div class="col-sm-6 p">
						<input class="form-control inputT" type='text' name='ctname' maxlength='50' placeholder='John (optional)'></div>
			</div>			

			<div class="row">
			    <div class="col-sm-3 p">
						Contact Number:&nbsp;</div>
				<div class="col-sm-6 p">
						<input class="form-control inputT" type='text' name='contact' maxlength='20' placeholder='+639123456789 (optional)'></div>
			</div>

			<div class="row">
			    <div class="col-sm-3 p">
						Email:&nbsp;</div>
				<div class="col-sm-6 p">
						<input class="form-control inputT" type='email' name='email' maxlength='50' placeholder='username@gmail.com (optional)'></div>
			</div>
<hr>

						<h3 class="h">Peak Season</h3><br>

							<ul>	
							<h4>Weekday's prices:</h4>
							<ul>

			<div class="row">
				<div class="col-sm-2 p">
							<li>12hours rate:</div>
				<div class="col-sm-3 p">
							&emsp;&emsp;Daytime:</div>
				<div class="col-sm-2 p">
							<input class="form-control inputT" placeholder="₱" type='number' name='pDay12Day' max="99999" required maxlength='5'></div>
				<div class="col-sm-2 p">
							&emsp;&emsp;Overnight:</div>
				<div class="col-sm-2 p">
							<input class="form-control inputT" placeholder="₱" type='number' name='pDay12Night' max="99999" required maxlength='5'></div>
							</li>
			</div>



			<div class="row">
				<div class="col-sm-2 p">
							<li>24hours rate:</div>
				<div class="col-sm-3 p">
							&emsp;&emsp;Daytime/Overnight:</div>
				<div class="col-sm-4 p">
							<input class="form-control inputT" placeholder="₱" type='number' name='pDay24' max="99999" required maxlength='5'></div>
							</li>
			</div>
							</ul>

							<h4>Weekend's prices:</h4>
							<ul>
			<div class="row">
				<div class="col-sm-2 p">
							<li>12hours rate:</div>
				<div class="col-sm-3 p">
							&emsp;&emsp;Daytime:</div>
				<div class="col-sm-2 p">
							<input class="form-control inputT" placeholder="₱" type='number' name='pEnd12Day' max="99999" required maxlength='5'></div> 
				<div class="col-sm-2 p">
							&emsp;&emsp;Overnight:</div>
				<div class="col-sm-2 p">
							<input class="form-control inputT" placeholder="₱" type='number' name='pEnd12Night' max="99999" required maxlength='5'></div>
							</li>
			</div>
							


			<div class="row">
				<div class="col-sm-2 p">
							<li>24hours rate:</div>
				<div class="col-sm-3 p">
						&emsp;&emsp;Daytime/Overnight:</div>
				<div class="col-sm-4 p">
							<input class="form-control inputT" placeholder="₱" type='number' name='pEnd24' max="99999" required maxlength='5'></div>
							</li>
			</div>
						</ul></ul>
						<h3 class="h">Off Season</h3><br>
							
							<ul>
							<h4>Weekday's prices:</h4>
							<ul>

			<div class="row">
				<div class="col-sm-2 p">
							<li>12hours rate:</div>
				<div class="col-sm-3 p">
							&emsp;&emsp;Daytime:</div>
				<div class="col-sm-2 p">
							<input class="form-control inputT" placeholder="₱" type='number' name='oDay12Day' max="99999" required maxlength='5'></div> 
				<div class="col-sm-2 p">
							&emsp;&emsp;Overnight:</div>
				<div class="col-sm-2 p">
							<input class="form-control inputT" placeholder="₱" type='number' name='oDay12Night' max="99999" required maxlength='5'></div>
							</li>
			</div>
							

			<div class="row">
				<div class="col-sm-2 p">
							<li>24hours rate:</div>
				<div class="col-sm-3 p">
						&emsp;&emsp;Daytime/Overnight:</div>
				<div class="col-sm-4 p">
							<input class="form-control inputT" placeholder="₱" type='number' name='oDay24' max="99999" required maxlength='5'></div>
							</li>
			</div>

							</ul>
							<h4>Weekend's prices:</h4>
							<ul>

			<div class="row">
				<div class="col-sm-2 p">
							<li>12hours rate:</div>
				<div class="col-sm-3 p">
							&emsp;&emsp;Daytime:</div>
				<div class="col-sm-2 p">
							<input class="form-control inputT" placeholder="₱" type='number' name='oEnd12Day' max="99999" required maxlength='5'></div> 
				<div class="col-sm-2 p">
							&emsp;&emsp;Overnight:</div>
				<div class="col-sm-2 p">
							<input class="form-control inputT" placeholder="₱" type='number' name='oEnd12Night' max="99999" required maxlength='5'></div>
							</li>
			</div>
							

			<div class="row">
				<div class="col-sm-2 p">				
							<li>24hours rate:</div>
				<div class="col-sm-3 p">
						&emsp;&emsp;Daytime/Overnight:</div>
				<div class="col-sm-4 p">
							<input class="form-control inputT" placeholder="₱" type='number' name='oEnd24' max="99999" required maxlength='5'></div>
							</li>
			</div>
					</ul></ul>
<hr>

					<h3>Pools:</h3>
						<ul><h4>Adult Pool(s)
						Quantity: <select name="adultQuantity" id="adultQuantity" required onchange="display()">
						  			<option value="1">1 <option value="2">2 <option value="3">3
								  </select></h4>
						<ul>
							<h4>Feet: <input class="inputT" type="number" name="aFirstFeet" min="4" max="10" step="0.5" required>&emsp;
							Slide:<input type="checkbox" name="aFirstSlide" value="with Slide">&emsp;&emsp;
							Cold: <input type="radio" name="aFirstTemp" value="Cold" required>&emsp;
							Warm: <input type="radio" name="aFirstTemp" value="Warm" required>&emsp;
							Hot:  <input type="radio" name="aFirstTemp" value="Hot"  required><br><br>

							<div id="aSecond">
								Feet: <input  class="inputT" type="number" name="aSecondFeet" min="4" max="10" step="0.5">&emsp;
								Slide: <input type="checkbox" name="aSecondSlide" value="with Slide">&emsp;&emsp;
								Cold: <input type="radio" name="aSecondTemp" value="Cold">&emsp;
								Warm: <input type="radio" name="aSecondTemp" value="Warm">&emsp;
								Hot:  <input type="radio" name="aSecondTemp" value="Hot"><br><br>
							</div>

							<div id="aThird">
								Feet: <input  class="inputT" type="number" name="aThirdFeet" min="4" max="10" step="0.5">&emsp;
								Slide: <input type="checkbox" name="aThirdSlide" value="with Slide">&emsp;&emsp;
								Cold: <input type="radio" name="aThirdTemp" value="Cold">&emsp;
								Warm: <input type="radio" name="aThirdTemp" value="Warm">&emsp;
								Hot:  <input type="radio" name="aThirdTemp" value="Hot"><br><br>
							</div>
							</h4>
						</ul>
						</ul>

						<ul><h4>Kiddie Pool(s)
						Quantity: <select name="kidQuantity" id="kidQuantity" onchange="display()">
						  <option value="null"><option value="1">1 <option value="2">2 <option value="3">3
						</select>
						<ul>
							<div id="kFirst"><br>
								Feet: <input class="inputT" type="number" name="kFirstFeet" min="1" max="10" step="0.5">&emsp;
								Slide: <input type="checkbox" name="kFirstSlide" value="with Slide">&emsp;&emsp;
								Cold: <input type="radio" name="kFirstTemp" value="Cold">&emsp;
								Warm: <input type="radio" name="kFirstTemp" value="Warm">&emsp;
								Hot:  <input type="radio" name="kFirstTemp" value="Hot"><br><br>
							</div>

							<div id="kSecond">
								Feet: <input  class="inputT" type="number" name="kSecondFeet" min="1" max="10" step="0.5">&emsp;
								Slide: <input type="checkbox" name="kSecondSlide" value="with Slide">&emsp;&emsp;
								Cold: <input type="radio" name="kSecondTemp" value="Cold">&emsp;
								Warm: <input type="radio" name="kSecondTemp" value="Warm">&emsp;
								Hot:  <input type="radio" name="kSecondTemp" value="Hot"><br><br>
							</div>

							<div id="kThird">				
								Feet: <input class="inputT" type="number" name="kThirdFeet" min="1" max="10" step="0.5">&emsp;
								Slide: <input type="checkbox" name="kThirdSlide" value="with Slide">&emsp;&emsp;
								Cold: <input type="radio" name="kThirdTemp" value="Cold">&emsp;
								Warm: <input type="radio" name="kThirdTemp" value="Warm">&emsp;
								Hot:  <input type="radio" name="kThirdTemp" value="Hot"><br><br>
							</div>
							</h4>
						</ul>
						</ul>
<hr>
		<div class="row">
    		<div class="col-sm-3 p">
						Pax can accomodate:</div>
			<div class="col-sm-3 p">
						<input class="form-control inputT" type='text' name='pax' required maxlength='10' placeholder="e.g. 10-20"></div>
		</div>

		<div class="row">
	    	<div class="col-sm-3 p">
						Bedrooms:</div>
			<div class="col-sm-2 p">
						<input class="form-control inputT" type="number" name="bedroom" min="1" max="100"  required></div>
			<div class="col-sm-1 p">
						AirCon:</div>
			<div class="col-sm-2 p">
						<input class="form-control inputT" type="number" name="aircon" min="0" max="100" required></div>
		</div>


		<div class="row">
    		<div class="col-sm-3 p">
						Sleeping Capacity:</div>
			<div class="col-sm-3 p">
						<input class="form-control inputT" type='text' name='sleep' required maxlength='10' placeholder="e.g. 10-20"></div>
		</div>

<hr>

		<div class="row">
    		<div class="col-sm-3 p">
						Facilities:</div>
			<div class="col-sm-6 p">
						<input class="form-control inputT" type='text' name='facilities' placeholder='e.g. Parking area, Function Hall, Kitchen' required></div>
		</div>

		<div class="row">
    		<div class="col-sm-3 p">
						Amenities:</div>
			<div class="col-sm-6 p">
						<input class="form-control inputT" type='text' name='amenities' placeholder='e.g. Videoke, Refrigerator, Bbq griller, Wifi' required></div>
		</div>


		<div class="row">
	    	<div class="col-sm-3 p">
						Other Expenses:</div>
			<div class="col-sm-6 p">
						<input class="form-control inputT" type='text' name='otherexpenses' placeholder='e.g. Gas Stove-300'></div>
		</div>

<hr>
<input type="file" accept=".jpg" name="files[]" multiple="multiple" required>

<div align="right"><input type='submit' name='submit' value='Submit'>&emsp;<input type='reset' value='Clear all'></div>
<br><br>
	</div>
</form>
</div>
</body>
<script src="quantity.js"></script>
</html>