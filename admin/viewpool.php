<?php
ob_start();
session_start();
?>

<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="styles/adminview.css">
</head>
<?php
	require_once("menunav.php");
?>
<script src="menu.js"></script>
</html>

<?php
if(isset($_SESSION['username']) && isset($_SESSION['password'])){
	// Get a connection for the database
	require_once('connection.php');

	// Create a query for the database
	$queryPool  = "SELECT poolId,poolName,kidQuantity,kFirstFeet,kFirstSlide,kFirstTemp, kSecondFeet,kSecondSlide,kSecondTemp, kThirdFeet,kThirdSlide,kThirdTemp, adultQuantity,aFirstFeet,aFirstSlide,aFirstTemp, aSecondFeet,aSecondSlide,aSecondTemp, aThirdFeet,aThirdSlide,aThirdTemp FROM pool";

	// Get a response from the database by sending the connection and the query
	$responsePool  = @mysqli_query($dbc,$queryPool);

	// If the query executed properly proceed
	if($responsePool){
		echo"<br><div class='title' align='center'>Pools Information</div><br>";
		echo'
		<table align="left" cellspacing="2" cellpadding="6" class="table">
		<tr class="tr">
			<th class="th"><b>Id</b></th>	
			<th class="th"><b>&emsp;&emsp;&emsp;Resort Name&emsp;&emsp;&emsp;</b></th>
			<th class="th"><b>Adult Pool Quantity</b></th>
			<th class="th"><b>Adult 1st Feet </b></th>
			<th class="th"><b>Adult 1st Slide</b></th>
			<th class="th"><b>Adult 1st Temp.</b></th>
			<th class="th"><b>Adult 2nd Feet</b></th>
			<th class="th"><b>Adult 2nd Slide</b></th>
			<th class="th"><b>Adult 2nd Temp.</b></th>
			<th class="th"><b>Adult 3rd Feet</b></th>
			<th class="th"><b>Adult 3rd Slide</b></th>
			<th class="th"><b>Adult 3rd Temp.</b></th>
			<th class="th"><b>Kiddie Pool Quantity</b></th>
			<th class="th"><b>Kiddie 1st Feet </b></th>
			<th class="th"><b>Kiddie 1st Slide</b></th>
			<th class="th"><b>Kiddie 1st Temp.</b></th>
			<th class="th"><b>Kiddie 2nd Feet</b></th>
			<th class="th"><b>Kiddie 2nd Slide</b></th>
			<th class="th"><b>Kiddie 2nd Temp.</b></th>
			<th class="th"><b>Kiddie 3rd Feet</b></th>
			<th class="th"><b>Kiddie 3rd Slide</b></th>
			<th class="th"><b>Kiddie 3rd Temp.</b></th>
			<th class="th"><b>Edit</b></th>
		</tr>';

		// mysqli_fetch_array will return a row of data from the query
		// until no further data is available
		while($dataPool = mysqli_fetch_array($responsePool)){
			$id = $dataPool['poolId'];
		echo '<tr class="tr">
			<td class="td">'.$dataPool['poolId'].'</td>
			<td class="td">'.$dataPool['poolName'].'</td> 
			<td class="td">'.$dataPool['adultQuantity'].'</td>
			<td class="td">'.$dataPool['aFirstFeet'].'</td>
			<td class="td">'.$dataPool['aFirstSlide'].'</td>
			<td class="td">'.$dataPool['aFirstTemp'].'</td>
			<td class="td">'.$dataPool['aSecondFeet'].'</td>
			<td class="td">'.$dataPool['aSecondSlide'].'</td>
			<td class="td">'.$dataPool['aSecondTemp'].'</td>
			<td class="td">'.$dataPool['aThirdFeet'].'</td>
			<td class="td">'.$dataPool['aThirdSlide'].'</td>
			<td class="td">'.$dataPool['aThirdTemp'].'</td>
			<td class="td">'.$dataPool['kidQuantity'].'</td>
			<td class="td">'.$dataPool['kFirstFeet'].'</td>
			<td class="td">'.$dataPool['kFirstSlide'].'</td>
			<td class="td">'.$dataPool['kFirstTemp'].'</td>
			<td class="td">'.$dataPool['kSecondFeet'].'</td>
			<td class="td">'.$dataPool['kSecondSlide'].'</td>
			<td class="td">'.$dataPool['kSecondTemp'].'</td>
			<td class="td">'.$dataPool['kThirdFeet'].'</td>
			<td class="td">'.$dataPool['kThirdSlide'].'</td>
			<td class="td">'.$dataPool['kThirdTemp'].'</td>
			<td align="left" class="td">'."<a href='updatepool.php?id=$id'>Update</a>".'</td>
		</tr>';
	}
	echo '</table>';
	} else {
		echo "Couldn't issue database query<br />";
		echo mysqli_error($dbc);
	}

	// Close connection to the database
	mysqli_close($dbc);
}
else{
	header("Location:login.php");
	exit();
}
ob_end_flush();
?>