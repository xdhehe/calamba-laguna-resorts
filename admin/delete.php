<?php
ob_start();
session_start();
?>

<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="styles/adminview.css">
</head>
<?php
	require_once("menunav.php");
?>
<script src="menu.js"></script>
</html>


<?php
if(isset($_SESSION['username']) && isset($_SESSION['password'])){
	require_once("connection.php");
	if(isset($_POST['submit'])){
		$id=trim($_POST['delete']);

		$queryDeletePrice = "DELETE FROM price WHERE priceId = '$id'";
		$queryDeletePool = "DELETE FROM pool WHERE poolId = '$id'";

		$queryGetName = "SELECT name FROM information WHERE id = '$id'";
		$responseGetname= mysqli_query($dbc, $queryGetName);
		$dataGetName = mysqli_fetch_array($responseGetname);
		$name = $dataGetName['name'];

		if((mysqli_query($dbc, $queryDeletePrice)) && (mysqli_query($dbc, $queryDeletePool))){

			$path= "../resorts/$name";
  			$files = scandir($path);
  			$files = array_diff(scandir($path), array('..', '.'));
  			$count = count($files);

  			for ($counter=2; $counter<=$count+1; $counter++)
    			unlink("../resorts/$name/$files[$counter]");
    		
			if(rmdir("../resorts/$name"))
  				echo"Directory removed.<br>";

			$queryDeleteInfo = "DELETE FROM information WHERE id = '$id'";

			if (mysqli_query($dbc, $queryDeleteInfo))
			    echo "Resort deleted successfully<br>";



		}
		else 
		    echo "Error deleting record: " . $dbc->error;
	}//end of delete function
}//end of checking if login
else{
	header("Location:login.php");
	exit();
}

?>


<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="styles/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="styles/adminview.css">	
	<title>DELETE RESORT</title>
</head>
<body>
<div>
	<div class="title" align="center">Delete existing resort</div>
<br>
<div class="container">
	<div class="row">
		<div class="col-sm-6">
			<h4>NOTE:Be careful when deleting a resorts, it can't be UNDONE.</h4>
			<form method="post" action="delete.php"></div>
		<div class="col-sm-6">
			<div align="right">Enter the resort id that you want to delete:
			<input class="inputT"  type="text" placeholder="e.g. 3" name="delete">
			<input type="submit" value="Delete" name="submit"></div>
			</form></div>
	</div>
</div>
</div>
</body>
</html>

<?php
$queryInfo = "SELECT id,name,address,ctname,contact,time FROM information";
$responseInfo  = @mysqli_query($dbc, $queryInfo);
if($responseInfo){
	echo'
	<table align="left" cellspacing="2" cellpadding="6" class="table">
	<tr class="tr">
		<th class="th"><b>ID</b></th>
		<th class="th"><b>Resort Name</b></th>
		<th class="th"><b>Resort Address</b></th>
		<th class="th"><b>Care Taker Name</b></th>
		<th class="th"><b>Contact No.</b></th>
		<th class="th"><b>Time Added</b></th>
	</tr>';

while($dataInfo = mysqli_fetch_array($responseInfo)){
	echo '<tr class="tr">
		<td align="center"  class="td">'.$dataInfo['id'].'</td>
		<td align="center"  class="td">'.$dataInfo['name'].'</td>
		<td align="center"  class="td">'.$dataInfo['address'].'</td>
		<td align="center"  class="td">'.$dataInfo['ctname'].'</td>
 		<td align="center"  class="td">'.$dataInfo['contact'].'</td>
		<td align="center"  class="td">'.$dataInfo['time'].'</td>
	</tr>';
}
echo '</table>';
} else {
	echo "Couldn't issue database query<br />";
	echo mysqli_error($dbc);
}
mysqli_close($dbc);
ob_end_flush();
?>