<head>
	<link rel="stylesheet" type="text/css" href="styles/bootstrap.css">
  	<link rel="stylesheet" type="text/css" href="styles/menustyle.css">
  	<meta name="viewport" content="width=device-width, initial-scale=1">

  <script src="styles/jquery.min.js"></script>
  <script src="styles/bootstrap.min.js"></script>
</head>
<body>
<nav class="navbar navbar-default">
  <div class="container-fluid size">
	<div class="navbar-header">
							
							<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#clps" aria-expanded="false">
							<h4><img src="pics/menu.png" class="sIcon"></h4>
							</button>


	</div>
	<div class="collapse navbar-collapse" id="clps">
	<ul class="nav navbar-nav">

	    <li><a data-toggle="modal" data-target="#menuModal">&emsp;<b>Menu</b></a>

						<!-- Modal -->
					<div id="menuModal" data-keyboard="false" data-backdrop="static" class="modal fade">
					  <div class="modal-dialog modal-lg modSize">
					  		<div class="row">
							<div class="col-sm-12">
					    <!-- Modal content-->
					    <div class="modal-content modContent">
					      <div class="modal-header modHeader" align="center">
					        <button type="button" class="close" data-dismiss="modal">&times;</button>
					        <h2 class="modal-title">Menu</h2>
					      </div>
					      <div class="modal-body modBody" align="left">
					       		<div class="row">
					<ul>
					<div class="col-sm-6 pad">
					<div class="panel-body">
						<li class="liT">View/Update Resort(s)</li>
							<ul type='disc' class="ul">
								<li class="li"><a href='viewinfo.php'>Information about resort(s)</a></li>
								<li class="li"><a href='viewpool.php'>Pool information about resort(s)</a></li>
								<li class="li"><a href='viewpicture.php'>Pictures of resort(s)</a></li>
								<li class="li"><a href='viewprice.php'>Price information about resort(s)</a></li>
							</ul>	
					</div><!-- panel body -->
					</div><!-- end col 1 -->	
					<div class="col-sm-6 pad">
					<div class="panel-body">
						<li class="liT">Add/Delete</li>
							<ul type='disc' class="ul">
								<li class="li"><a href='delete.php'>Delete resort(s)</a></li>
								<li class="li"><a href='add.php'>Add another resort</a></li>
							</ul>
						<li class="liT">Others</li>
							<ul type='disc' class="ul">
								<li class="li"><a href='owner.php'>Pending Resort(s)</a></li>
								<li class="li"><a href='reservation.php'>Reservation(s)</a></li>
								<li class="li"><a href='backup.php'>Backup SQL</a></li>
							</ul>
					</div><!-- panel body -->
					</div><!-- end col 2 -->
				</div><!-- end row 2-->					  
					      </div>
					      <div class="modal-footer modFooter">
					        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					      </div>
					    </div>
					    	</div><!-- col -->
							</div><!-- row -->		  
					  </div>
					</div>
	    </li>
	</ul>
	  
	<ul class="nav navbar-nav navbar-right">
	    <li><a href="login.php">Log-out</a></li>
	</ul>
	</div>
  </div>
</nav>
</body>