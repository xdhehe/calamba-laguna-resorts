<?php
ob_start();
session_start();
?>

<!DOCTYPE html>
<html>
<head>
 	<link rel="stylesheet" type="text/css" href="styles/bootstrap.css">
  	<link rel="stylesheet" type="text/css" href="styles/menustyle.css">
  	<style>
	<?php require_once("styles/adminview.css");?>
</style>
</head>
<?php
	require_once("menunav.php");
?>
<script src="menu.js"></script>
</html>

<?php
if(isset($_SESSION['username']) && isset($_SESSION['password'])){
	require_once('connection.php');

	
	if(!empty($_GET['delete'])){
		$id=$_GET['delete'];

		$queryDeletePrice = "DELETE FROM pendingprice WHERE priceId = '$id'";
		$queryDeletePool =  "DELETE FROM pendingpool WHERE poolId = '$id'";

		$queryGetName = "SELECT name FROM pendinginformation WHERE id = '$id'";
		$responseGetname= mysqli_query($dbc, $queryGetName);
		$dataGetName = mysqli_fetch_array($responseGetname);
		$name = $dataGetName['name'];

		if((mysqli_query($dbc, $queryDeletePrice)) && (mysqli_query($dbc, $queryDeletePool))){

			$path= "../pending/$name";
  			$files = scandir($path);
  			$files = array_diff(scandir($path), array('..', '.'));
  			$count = count($files);

  			for ($counter=2; $counter<=$count+1; $counter++)
    			unlink("../pending/$name/$files[$counter]");
    		
			if(rmdir("../pending/$name"))
  				echo"Directory removed.<br>";

			$queryDeleteInfo = "DELETE FROM pendinginformation WHERE id = '$id'";

			if (mysqli_query($dbc, $queryDeleteInfo))
			    echo "Resort deleted successfully<br>";

		}
		else 
		    echo "Error deleting record: " . $dbc->error;
	}

	$queryOwner = "SELECT * FROM pendinginformation";
	$responseOwner  = @mysqli_query($dbc, $queryOwner);
	if($responseOwner){
		echo"<br><div class='title' align='center'>Resort's owner(s)</div><br>";
		echo'<table align="left" cellspacing="2" cellpadding="6" class="table">
		<tr>
			<th class="th"><b>Resort Name</b></th>
			<th class="th"><b>Owner Name</b></th>
			<th class="th"><b>Address</b></th>
			<th class="th"><b>Contact</b></th>
			<th class="th"><b>Email</b></th>
			<th class="th"><b>Time Added</b></th>
			<th class="th"><b>More info<b/></th>
			<th class="th"><b>Confirm?<b/></th>
			<th class="th"><b>Delete?<b/></th>
		</tr>';

		while($dataOwner = mysqli_fetch_array($responseOwner)){
			$id = $dataOwner['id'];
		echo '<tr class="tr">
			<td class="td">'.$dataOwner['name'].'</td>
			<td class="td">'.$dataOwner['ownername'].'</td>
			<td class="td">'.$dataOwner['address'].'</td>
			<td class="td">'.$dataOwner['contact'].'</td>
			<td class="td">'.$dataOwner['email'].'</td>
			<td class="td">'.$dataOwner['time'].'</td>
			<td class="td">'."<a href='moredetails.php?id=$id'>Review Details</a>".'</td>
			<td class="td">'."<a href='accept.php?id=$id'>Accept</a>".'</td>
			<td class="td">'."<a href='owner.php?delete=$id'>Reject</a>".'</td>
		</tr>';
	}
	echo '</table>';
	} else {
		echo "Couldn't issue database query<br />";
		echo mysqli_error($dbc);
	}
	mysqli_close($dbc);// Close connection to the database
}
else{
	header("Location:login.php");
	exit();
}
ob_end_flush();
?>