<?php
ob_start();
session_start();
?>

<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" type="text/css" href="styles/upload.css">
  <link rel="stylesheet" type="text/css" href="styles/bootstrap.css">
  <title>Update Resort Picture</title>
</head>
<body>
<?php
  require_once("menunav.php");
?>
<script src="menu.js"></script>



<?php 
require_once("../admin/connection.php");

if(isset($_SESSION['ownerusername']) && isset($_SESSION['ownerpassword'])){

if(!empty($_GET['id'])){
	$id = $_GET['id'];
	$_SESSION['id'] =$id;
}
else
	$id = $_SESSION['id'];

$queryInfoView = "SELECT name FROM information where id = '$id'";
$responseInfoView  = @mysqli_query($dbc, $queryInfoView);
$dataInfoView = mysqli_fetch_array($responseInfoView);	

$name = $dataInfoView['name'];


if(!empty($_GET['clear'])){
  $checkDir = "../resorts/$name";
  if (count(glob("$checkDir/*")) === 0 ) { 
    echo"Folder is already empty<br>Try hitting Ctrl+ F5";

  }
  else{
    $path= "../resorts/$name";
    $files = scandir($path);
    $files = array_diff(scandir($path), array('..', '.'));
    $count = count($files);

    for ($counter=2; $counter<=$count+1; $counter++)
      unlink("../resorts/$name/$files[$counter]");

    if(rmdir("../resorts/$name")){
        echo"Pictures cleared.";
        header("location:updatepicture.php?id=$id");
        exit();
    }
  }
}

//ADDING ALL THE PICTURE
if(isset($_POST['submit'])){
  if(mkdir("../resorts/$name")){
    echo"Directory created";
    require_once("uploadupdate.php");
    header("location:updatepicture.php");
  }
}
mysqli_close($dbc);// Close connection to the database
}//end of checking if login
else{
  header("Location:login.php");
  exit();
}

ob_end_flush();
?>


<div class="container">
<div align="center" class="title">Update your resort pictures.</div>    
  <div class="row">
  <hr>
    <div class="col-sm-12" align="center"><h4>You're currently updating resort picture of: <?php echo $dataInfoView['name']; ?></h4></div>
  </div>
  <div class="row">
  <div class="col-sm-12" align="center">
<?php
    for ($counter=1; $counter<=4; $counter++) { 
     echo "<img src='../resorts/$name/$counter.jpg'  style='height:150px; width:auto; max-width:200px; border-radius: 10px; padding: 5px;'>";
    }
?>
<br>
<?php
    for ($counter=5; $counter<=8; $counter++) { 
     echo "<img src='../resorts/$name/$counter.jpg' style='height:150px; width:auto; max-width:200px; border-radius: 10px; padding: 5px;'>";
    }
?>
<br>
<?php
    for ($counter=9; $counter<=12; $counter++) { 
     echo "<img src='../resorts/$name/$counter.jpg' style='height:150px; width:auto; max-width:200px; border-radius: 10px; padding: 5px;'>";
    }
?>
  </div><!-- col 2 -->
  </div><!-- end row 2 --><br>
  <form action='updatepicture.php' method="post" enctype="multipart/form-data">
  <div class="row">
  <div class="col-sm-6">
  <a class="btn btn-info" href="updatepicture.php?clear=clear">Clear Pictures</a>
  </div><!-- end col -->
  <div class="col-sm-6" align="right">
  <input class="btn btn-success" type="file" accept=".jpg" name="files[]" multiple="multiple" required>
  </div><!-- end col -->
  </div><!-- end row -->
	<hr>
	 <div align="right"><input class="btn btn-default" type="submit" name="submit"></div>
  </form>



</div><!-- end of div container -->
<br><br>
</body>
</html>