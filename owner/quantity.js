"use strict";
var kFirst = document.getElementById("kFirst");
var kSecond = document.getElementById("kSecond");
var kThird = document.getElementById("kThird");

var aSecond = document.getElementById("aSecond");
var aThird = document.getElementById("aThird");

kFirst.style.display = "none";
kSecond.style.display = "none";
kThird.style.display = "none";

aSecond.style.display = "none";
aThird.style.display = "none";


function display(){
	var kid   = document.getElementById("kidQuantity").value;
	var adult = document.getElementById("adultQuantity").value; 

	if(kid=="null"){
		kFirst.style.display = "none";
		kSecond.style.display = "none";
		kThird.style.display = "none";
	}

	if(kid==1){
		kFirst.style.display = "block";
		kSecond.style.display = "none";
		kThird.style.display = "none";
	}

	if(kid==2){
		kFirst.style.display = "block";
		kSecond.style.display= "block";
		kThird.style.display =  "none";
	}
	if(kid==3){
		kFirst.style.display = "block";
		kSecond.style.display = "block";
		kThird.style.display = "block";
	}

	if(adult==1){
		aSecond.style.display = "none";
		aThird.style.display = "none";
	}

	if(adult==2){
		aSecond.style.display = "block";
		aThird.style.display = "none";
	}
	if(adult==3){
		aSecond.style.display="block";
		aThird.style.display = "block";
	}
}
