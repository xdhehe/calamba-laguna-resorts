<?php
ob_start();
session_start();
?>

<!DOCTYPE html>
<html>
<head>
	<title>Owner Pending Resorts</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="styles/adminview.css">
</head>
</html>
<?php
  require_once("menunav.php");
?>
<script src="menu.js"></script>

<?php
if(isset($_SESSION['ownerusername']) && isset($_SESSION['ownerpassword'])){ // Check if login
	require_once("../admin/connection.php");

	$ownername = $_SESSION['ownerusername'];
	$queryInfoPending = "SELECT * FROM pendinginformation WHERE ownername = '$ownername'";
	$responseInfoPending = mysqli_query($dbc,$queryInfoPending);

	if($responseInfoPending){

		echo"<br><div class='title' align='center'>Resort's Pending Information<br>";
		echo"<br><i><h4>Please wait, we will notify you when your resort is posted.</i></h4></div><br>";
		echo'<table align="left" cellspacing="2" cellpadding="6" class="table">
		<tr>
			<th class="th"><b>ID</b></th>
			<th class="th"><b>Resort Name</b></th>
			<th class="th"><b>Resort Address</b></th>
			<th class="th"><b>Time Added</b></th>
		</tr>';

		// mysqli_fetch_array will return a row of data from the query
		// until no further data is available
		while($dataInfoPending = mysqli_fetch_array($responseInfoPending)){
		echo '<tr class="tr">
			<td class="td">'.$dataInfoPending['id'].'</td>
			<td class="td">'.$dataInfoPending['name'].'</td>
			<td class="td">'.$dataInfoPending['address'].'</td>
			<td class="td">'.$dataInfoPending['time'].'</td>
		</tr>';
	}
	echo '</table>';
	} else {
		echo "Couldn't issue database query<br />";
		echo mysqli_error($dbc);
	}
	mysqli_close($dbc);// Close connection to the database
}
else{
	header("Location:login.php");
	exit();
}
ob_end_flush();
?>