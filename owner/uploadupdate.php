<?php
$errors = array();
$uploadedFiles = array();
$extension = array("jpg");

$counter = 0;

foreach($_FILES["files"]["tmp_name"] as $key=>$tmp_name){
	$temp = $_FILES["files"]["tmp_name"][$key];
	$fileName = $_FILES["files"]["name"][$key];

	if(empty($temp))
	{
		break;
	}
	
	$counter++;
	$UploadOk = true;
	
	$ext = pathinfo($fileName, PATHINFO_EXTENSION);
	if(in_array($ext, $extension) == false){
		$UploadOk = false;
		array_push($errors, $fileName." is invalid file type.");
	}
	
	if(file_exists("../resorts/$name"."/".$fileName) == true){
		$UploadOk = false;
		array_push($errors, $fileName." file is already exist.");
	}
	
	if($UploadOk == true){
		move_uploaded_file($temp,"../resorts/$name"."/".$fileName);
		array_push($uploadedFiles, $fileName);
	}
}

if($counter>0){
	if(count($errors)>0)
	{
		echo "<b>Errors:</b>";
		echo "<br/><ul>";
		foreach($errors as $error)
		{
			echo "<li>".$error."</li>";
		}
		echo "</ul><br/>";
	}
	
	if(count($uploadedFiles)>0){
		echo "<b>Uploaded Files:</b>";
		echo "<br/><ul>";
		foreach($uploadedFiles as $fileName)
		{
			echo "<li>".$fileName."</li>";
		}
		echo "</ul><br/>";
		
		echo count($uploadedFiles)." file(s) are successfully uploaded.";
	}								
}
else{
	echo "Please, Select file(s) to upload.";
}
?>