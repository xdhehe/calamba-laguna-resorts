<?php
ob_start();
session_start();
if(isset($_SESSION['ownerusername']) && isset($_SESSION['ownerpassword'])){?><!-- Check if login -->

<!DOCTYPE html>
<html>
<head>
	<title>Owner Menu</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="styles/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="styles/menu.css">

</head>
<body>
<div class="container">
<div class="row">
<div class="col-sm-12" align="center">

	<h1>Hello! <?php echo $_SESSION['fullname']; ?> Welcome to your account.</h1><br>
<hr>
</div><!-- col -->
</div><!-- row -->
<div class="row" align="center">
<div class="col-sm-6">

						<p class="liT">View/Update your Resort(s)</p>
							<ul type='disc' class="ul">
								<li class="li"><a href="viewinfo.php">Information about resort(s)</a></li>
								<li class="li"><a href="viewpool.php">Pool information about resort(s)</a></li>
								<li class="li"><a href="viewpicture.php">Pictures of resort(s)</a></li>
								<li class="li"><a href="viewprice.php">Price information about resort(s)</a></li>
							</ul>	

</div><!-- col -->
<div class="col-sm-6">
						<p class="liT">Add/Pending Resort(s)</p>
							<ul type='disc' class="ul">
								<li class="li"><a href="add.php">Add another Resort</a></li>
								<li class="li"><a href="pending.php">Pending Resort</a></li>
							</ul>
						

</div><!-- col -->
</div><!-- row -->
<div class="row">
<div class="col-sm-12" align="right">
<hr>
	<a class="btn btn-md btn-default" href="logout.php">Logout</a>
</div><!-- col -->
</div><!-- row -->
</div><!-- container -->
</body>
</html>

<?php }

else{
	header("Location:login.php");
	exit();
}
ob_end_flush();

?>	