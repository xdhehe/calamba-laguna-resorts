<?php
ob_start();
session_start();
?>


<!DOCTYPE html>
<html>
<head>
	<title>Resorts</title>
	<link rel="stylesheet" type="text/css" href="styles/adminview.css">
</head>
<?php
	require_once("menunav.php");
?>
<script src="menu.js"></script>
</html>

<?php
if(isset($_SESSION['ownerusername']) && isset($_SESSION['ownerpassword'])){
	
	require_once('../admin/connection.php');
	$resortowner = $_SESSION['ownerusername'];
	// Create a query for the database
	$queryInfo = "SELECT id,name,address,contact,time FROM information WHERE ownername = '$resortowner'";

	// Get a response from the database by sending the connection and the query
	$responseInfo  = @mysqli_query($dbc, $queryInfo);

	// If the query executed properly proceed
	if($responseInfo){

		echo"<br><div class='title' align='center'>Resort's Information</div><br>";
		echo'<table align="left" cellspacing="2" cellpadding="6" class="table">
		<tr>
			<th class="th"><b>ID</b></th>
			<th class="th"><b>Resort Name</b></th>
			<th class="th"><b>Resort Address</b></th>
			<th class="th"><b>Time Added</b></th>
			<th class="th"><b>Update</b></th>
		</tr>';

		// mysqli_fetch_array will return a row of data from the query
		// until no further data is available
		while($dataInfo = mysqli_fetch_array($responseInfo)){
			$id = $dataInfo['id'];
		echo '<tr class="tr">
			<td class="td">'.$id.'</td>
			<td class="td">'.$dataInfo['name'].'</td>
			<td class="td">'.$dataInfo['address'].'</td>
			<td class="td">'.$dataInfo['time'].'</td>
			<td class="td">'."<a href='updatepicture.php?id=$id'>Update Picture(s)</a>".'</td>
		</tr>';
	}
	echo '</table>';
	} else {
		echo "Couldn't issue database query<br />";
		echo mysqli_error($dbc);
	}
	mysqli_close($dbc);// Close connection to the database
}
else{
	header("Location:login.php");
	exit();
}
ob_end_flush();
?>