<?php
ob_start();
session_start();
?>

<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="styles/adminview.css">
</head>
<?php
	require_once("menunav.php");
?>
<script src="menu.js"></script>
</html>



<?php 
require_once('../admin/connection.php');

if(isset($_SESSION['ownerusername']) && isset($_SESSION['ownerpassword'])){

if(!empty($_GET['id'])){
	$id = $_GET['id'];
	$_SESSION['id'] =$id;
}
else
	$id = $_SESSION['id'];	

$queryPriceView = "SELECT priceId,priceName,pDay12Day, pDay12Night, pDay24, pEnd12Day, pEnd12Night, pEnd24,
 oDay12Day, oDay12Night, oDay24, oEnd12Day, oEnd12Night, oEnd24 FROM price where priceId = '$id'";

$responsePriceView  = @mysqli_query($dbc, $queryPriceView);
$dataPriceView = mysqli_fetch_array($responsePriceView);	


	if(isset($_POST["update"])){

		$pDay12Day    = trim($_POST['pDay12Day']);
		$pDay12Night  = trim($_POST['pDay12Night']);
		$pDay24       = trim($_POST['pDay24']);							
		$pEnd12Day    = trim($_POST['pEnd12Day']);										
		$pEnd12Night  = trim($_POST['pEnd12Night']);				
		$pEnd24       = trim($_POST['pEnd24']);
		$oDay12Day    = trim($_POST['oDay12Day']);
		$oDay12Night  = trim($_POST['oDay12Night']);
		$oDay24       = trim($_POST['oDay24']);							
		$oEnd12Day    = trim($_POST['oEnd12Day']);										
		$oEnd12Night  = trim($_POST['oEnd12Night']);				
		$oEnd24       = trim($_POST['oEnd24']);

			
	$qPrice = "UPDATE price SET pDay12Day='$pDay12Day',pDay12Day= '$pDay12Day', pDay12Night= '$pDay12Night',
    pDay24= '$pDay24', pEnd12Day= '$pEnd12Day', pEnd12Night= '$pEnd12Night', pEnd24= '$pEnd24',
oDay12Day= '$oDay12Day', oDay12Night= '$oDay12Night', oDay24= '$oDay24', oEnd12Day= '$oEnd12Day', oEnd12Night= '$oEnd12Night', oEnd24= '$oEnd24' WHERE priceId ='$id'";

	$qInfo = "UPDATE information SET infoODay12Day= '$oDay12Day' WHERE id ='$id'";

		if (mysqli_query($dbc, $qPrice) && mysqli_query($dbc, $qInfo)){
	    	echo "Updated successfully<br>";
	    	header("Location:viewprice.php");
	    } 
		else 
		    echo mysqli_error($dbc);
	}// end of updating function
	else
		echo mysqli_error($dbc);
mysqli_close($dbc);// Close connection to the database
}//end of checking if login
else{
	header("Location:login.php");
	exit();
}

ob_end_flush();
?>



<!DOCTYPE html>
<html>
<head>
	<title>Update Resort Price</title>
	<link rel="stylesheet" type="text/css" href="styles/update.css">
  	<link rel="stylesheet" type="text/css" href="styles/bootstrap.css">
</head>
<body>
<div class="container">
<div align="center" class="title">Update your resort prices</div>
<hr>
<form action="updateprice.php" method="post">					
		<div class="row">
			<div class="col-sm-5 p">
					You're currently updating resort: <?php echo $dataPriceView['priceName']; ?></div>
		</div>
	<hr>
					<h3 class="h">Peak Season</h3><br>
		
						<ul>
						<h4>Weekday's prices:</h4>
						<ul>
						
		<div class="row">

			<div class="col-sm-2 p">					
						<li>12hours rate:</div>
			<div class="col-sm-3 p">
						 &emsp;&emsp;Daytime:</div>
			<div class="col-sm-2 p">
						 <input class="form-control inputT" type='number' name='pDay12Day' required maxlength='5' max="99999" value="<?php echo $dataPriceView['pDay12Day'];?>"></div>
			<div class="col-sm-2 p">
						 &emsp;&emsp;Overnight:</div>
			<div class="col-sm-2 p">
						 <input class="form-control inputT" type='number' name='pDay12Night' required maxlength='5' max="99999" value="<?php echo $dataPriceView['pDay12Night'];?>"></div>
						 </li>
		</div>


		<div class="row">
			<div class="col-sm-2 p">				
						<li>24hours rate:</div> 
			<div class="col-sm-3 p">
						&emsp;&emsp;Daytime/Overnight:</div>
			<div class="col-sm-4 p">
						<input class="form-control inputT" type='number' name='pDay24' required maxlength='5' max="99999" value="<?php echo $dataPriceView['pDay24'];?>"></div>
						</li>			
		</div>
						</ul>				



						<h4>Weekend's prices:</h4>

						<ul>
		<div class="row">
			<div class="col-sm-2 p">									
						<li>12hours rate:</div> 
			<div class="col-sm-3 p">
						&emsp;&emsp;Daytime:</div>
			<div class="col-sm-2 p">
						<input class="form-control inputT" type='number' name='pEnd12Day' required maxlength='5' max="99999" value="<?php echo $dataPriceView['pEnd12Day'];?>"></div>
			<div class="col-sm-2 p">
						&emsp;&emsp;Overnight:</div>
			<div class="col-sm-2 p">
						<input class="form-control inputT" type='number' name='pEnd12Night' required maxlength='5' max="99999" value="<?php echo $dataPriceView['pEnd12Night'];?>"></div>
						</li>
		</div>

		<div class="row">
			<div class="col-sm-2 p">				
						<li>24hours rate:</div>
			<div class="col-sm-3 p">
						&emsp;&emsp;Daytime/Overnight:</div>
			<div class="col-sm-4 p">
						<input class="form-control inputT" type='number' name='pEnd24' required maxlength='5' max="99999" value="<?php echo $dataPriceView['pEnd24'];?>"></div>
						</li>			
		</div>
					</ul></ul>					

					<h3 class="h">Off Season</h3><br>
						

						<ul>
						<h4>Weekday's prices:</h4>
						<ul>

		<div class="row">
			<div class="col-sm-2 p">					
						<li>12hours rate:</div>
			<div class="col-sm-3 p">
						&emsp;&emsp;Daytime:</div>
			<div class="col-sm-2 p">
						<input class="form-control inputT" type='number' name='oDay12Day' required maxlength='5' max="99999" value="<?php echo $dataPriceView['oDay12Day'];?>"></div> 
			<div class="col-sm-2 p">
						&emsp;&emsp;Overnight:</div>
			<div class="col-sm-2 p">
						<input class="form-control inputT" type='number' name='oDay12Night' required maxlength='5' max="99999" value="<?php echo $dataPriceView['oDay12Night'];?>"></div>
						</li>
		</div>
						


		<div class="row">
			<div class="col-sm-2 p">				
						<li>24hours rate:</div>
			<div class="col-sm-3 p">
						&emsp;&emsp;Daytime/Overnight:</div>
			<div class="col-sm-4 p">
						<input class="form-control inputT" type='number' name='oDay24' required maxlength='5' max="99999" value="<?php echo $dataPriceView['oDay24'];?>"></div>
						</li>			
		</div>
						</ul>
						<h4>Weekend's prices:</h4>
						<ul>

		<div class="row">
			<div class="col-sm-2 p">									
						<li>12hours rate:</div>
			<div class="col-sm-3 p">
						&emsp;&emsp;Daytime:</div>
			<div class="col-sm-2 p">
						<input class="form-control inputT" type='number' name='oEnd12Day' required maxlength='5' max="99999" value="<?php echo $dataPriceView['oEnd12Day'];?>"></div>
			<div class="col-sm-2 p">
						&emsp;&emsp;Overnight:</div>
			<div class="col-sm-2 p">
						<input class="form-control inputT" type='number' name='oEnd12Night' required maxlength='5' max="99999" value="<?php echo $dataPriceView['oEnd12Night'];?>"></div>
						</li>
		</div>
						

		<div class="row">
			<div class="col-sm-2 p">				
						<li>24hours rate:</div>
			<div class="col-sm-3 p">
						&emsp;&emsp;Daytime/Overnight:</div>
			<div class="col-sm-4 p">
						<input class="form-control inputT" type='number' name='oEnd24' required maxlength='5' max="99999" value="<?php echo $dataPriceView['oEnd24'];?>"></div>
						</li>			
		</div>
						</ul></ul>	
<hr>
<div align="right"><input type="submit" name="update" value="Update"></div>
<br><br>
</form>
</div>
</body>
</html>