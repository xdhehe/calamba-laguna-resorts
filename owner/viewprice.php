<?php
ob_start();
session_start();
?>
<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="styles/adminview.css">
</head>
<?php
	require_once("menunav.php");
?>
<script src="menu.js"></script>
</html>

<?php
if(isset($_SESSION['ownerusername']) && isset($_SESSION['ownerpassword'])){
	
	require_once('../admin/connection.php');
	$resortowner = $_SESSION['ownerusername'];
	// Create a query for the database
	$queryPrice = "SELECT priceId,priceName,pDay12Day, pDay12Night, pDay24, pEnd12Day, pEnd12Night, pEnd24, oDay12Day, oDay12Night, oDay24, oEnd12Day, oEnd12Night, oEnd24 FROM price WHERE ownername = '$resortowner'";

	// Get a response from the database by sending the connection and the query
	$responsePrice = @mysqli_query($dbc, $queryPrice);

	// If the query executed properly proceed
	if($responsePrice){
		echo"<br><div class='title' align='center'>Resort's Prices</div><br>";
		echo"<div class='legends'>Legends:&emsp;&emsp;WD = Weekdays,&emsp;WE = Weekends,&emsp;DT = Daytime,&emsp;ON = Overnight</div>";
		echo'
		<table align="left" cellspacing="2" cellpadding="6" class="table">
		<tr class="tr">
			<th class="th"><b>Id</b></th>
			<th class="th"><b>&emsp;&emsp;&emsp;Resort Name&emsp;&emsp;&emsp;</b></th>
			<th class="th"><b>Peak WD 12hrs DT</b></th>		
			<th class="th"><b>Peak WD 12hrs ON</b></th>
			<th class="th"><b>Peak WD 24hrs</b></th>
			<th class="th"><b>Peak WE 12hrs DT</b></th>		
			<th class="th"><b>Peak WE 12hrs ON</b></th>
			<th class="th"><b>Peak WE 24hrs</b></th>

			<th class="th"><b>Off WD 12hrs DT</b></th>		
			<th class="th"><b>Off WD 12hrs ON</b></th>
			<th class="th"><b>Off WD 24hrs</b></th>
			<th class="th"><b>Off WE 12hrs DT</b></th>		
			<th class="th"><b>Off WE 12hrs ON</b></th>
			<th class="th"><b>Off WE 24hrs</b></th>
			<th class="th"><b>Edit</b></th>
		</tr>';

		// mysqli_fetch_array will return a row of data from the query
		// until no further data is available
		while($dataPrice = mysqli_fetch_array($responsePrice)){
			$id = $dataPrice['priceId'];
		echo '<tr class="tr">
			<td class="td">'.$dataPrice['priceId'].'</td>
			<td class="td">'.$dataPrice['priceName'].'</td>
			<td class="td">'.$dataPrice['pDay12Day'].'</td>
			<td class="td">'.$dataPrice['pDay12Night'].'</td>
			<td class="td">'.$dataPrice['pDay24'].'</td>
			<td class="td">'.$dataPrice['pEnd12Day'].'</td>
			<td class="td">'.$dataPrice['pEnd12Night'].'</td>
			<td class="td">'.$dataPrice['pEnd24'].'</td>
			<td class="td">'.$dataPrice['oDay12Day'].'</td>
			<td class="td">'.$dataPrice['oDay12Night'].'</td>
			<td class="td">'.$dataPrice['oDay24'].'</td>
			<td class="td">'.$dataPrice['oEnd12Day'].'</td>
			<td class="td">'.$dataPrice['oEnd12Night'].'</td>
			<td class="td">'.$dataPrice['oEnd24'].'</td>
			<td class="td">'."<a href='updateprice.php?id=$id'>Update</a>".'</td>
		</tr>';
	}
	echo '</table>';
	} else {
		echo "Couldn't issue database query<br />";
		echo mysqli_error($dbc);
	}

	// Close connection to the database
	mysqli_close($dbc);
}
else{
	header("Location:login.php");
	exit();
}

ob_end_flush();
?>